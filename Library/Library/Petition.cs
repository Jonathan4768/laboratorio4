﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Library
{
    public class Petition
    {
        private readonly HttpClient httpClient;

        public Petition()
        {
            this.httpClient = new HttpClient();
        }

        public async Task<(bool, string)> SendRequestAsync(string url, HttpMethod method, string cuerpoJson = null, List<string> datos = null, string username = null, string password = null)
        {
            try
            {
                if (datos != null)
                    cuerpoJson = ReplaceQuestionMarks(cuerpoJson, datos);

                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                {
                    var byteArray = Encoding.ASCII.GetBytes($"{username}:{password}");
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                }

                var request = new HttpRequestMessage(method, url);

                if (cuerpoJson != null)
                    request.Content = new StringContent(cuerpoJson, Encoding.UTF8, "application/json");

                var response = await httpClient.SendAsync(request);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var responseBody = await response.Content.ReadAsStringAsync();
                    return (true, responseBody); // Conexión exitosa y respuesta recibida
                }
                else
                {
                    return (false, $"Error al enviar la petición: {response.StatusCode}"); // Código de estado no esperado
                }
            }
            catch (HttpRequestException ex)
            {
                return (false, $"Error al enviar la petición: {ex.Message}"); // Error al enviar la solicitud
            }
            catch (Exception ex)
            {
                return (false, $"Error desconocido: {ex.Message}"); // Otro tipo de error
            }
        }

        private string ReplaceQuestionMarks(string json, List<string> datos)
        {
            if (string.IsNullOrEmpty(json))
                return json;

            int dataIndex = 0;

            return Regex.Replace(json, @"\?", m =>
            {
                if (dataIndex < datos.Count)
                {
                    string replacement = datos[dataIndex];
                    dataIndex++;
                    return replacement;
                }
                else
                {
                    return "?";
                }
            });
        }
    }
}