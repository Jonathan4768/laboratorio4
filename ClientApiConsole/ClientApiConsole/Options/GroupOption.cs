﻿using ClientApiConsole.Verifications;
using Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientApiConsole.Options
{
    public class GroupOption
    {
        private string text;
        private List<string> inputs;

        VerificationsUsername verificationsUsername = new VerificationsUsername();
        VerificationsPassword verificationsPassword = new VerificationsPassword();
        Petition petition = new Petition();
        

        public GroupOption(string text, List<string> inputs)
        {
            this.text = text;
            this.inputs = inputs;
            Menu();
        }

        private void Menu()
        {
            switch (text)
            {
                case "create-group":
                    CreateGroup();

                    break;
                case "delete-group":
                    DeleteGroup();

                    break;
                case "get-group":
                    GetGroup();

                    break;
                case "get-all-group":
                    GetAllGroup();

                    break;
            }
        }

        private async Task CreateGroup()
        {
            inputs[0] = inputs.Count > 0 ? inputs[0] : null;
            inputs[1] = inputs.Count > 0 ? inputs[1] : null;

            inputs[0] = GetValidInput("username", verificationsUsername.IsAllowedChars, inputs[0], 0);
            inputs[1] = GetValidInput("password", verificationsPassword.IsValidPassword, inputs[1], 1);

            string username = inputs[0];
            string password = inputs[1];

            string cuerpoJson = @"
            {
                ""title"": ""?"",
                ""userId"": {
                    ""value"": ""?""
                }
            }";

            string url = "http://localhost:5046/api/groups/create-group";

            inputs.RemoveRange(0, 2);

            var (conexionExitosa, respuesta) = await petition.SendRequestAsync(url, HttpMethod.Post, cuerpoJson, inputs, username, password);

            if (conexionExitosa)
            {
                Console.WriteLine("Conexión establecida correctamente.");

                // Si se recibe una respuesta
                if (!string.IsNullOrEmpty(respuesta))
                {
                    Console.WriteLine("Respuesta recibida:");
                    Console.WriteLine(respuesta);
                }
                else
                {
                    Console.WriteLine("No se recibió respuesta del servidor.");
                }
            }
            else
            {
                Console.WriteLine("Error al establecer la conexión o al enviar la solicitud:");
                Console.WriteLine(respuesta);
            }

        }

        private async Task DeleteGroup()
        {
            inputs[0] = inputs.Count > 0 ? inputs[0] : null;
            inputs[1] = inputs.Count > 0 ? inputs[1] : null;

            inputs[0] = GetValidInput("username", verificationsUsername.IsAllowedChars, inputs[0], 0);
            inputs[1] = GetValidInput("password", verificationsPassword.IsValidPassword, inputs[1], 1);

            string username = inputs[0];
            string password = inputs[1];

            inputs.RemoveRange(0, 2);

            string url = $"http://localhost:5046/api/groups/delete-group-by-id/{inputs[2]}";

            var (conexionExitosa, respuesta) = await petition.SendRequestAsync(url, HttpMethod.Delete, "", inputs, username, password);

            if (conexionExitosa)
            {
                Console.WriteLine("Conexión establecida correctamente.");

                // Si se recibe una respuesta
                if (!string.IsNullOrEmpty(respuesta))
                {
                    Console.WriteLine("Respuesta recibida:");
                    Console.WriteLine(respuesta);
                }
                else
                {
                    Console.WriteLine("No se recibió respuesta del servidor.");
                }
            }
            else
            {
                Console.WriteLine("Error al establecer la conexión o al enviar la solicitud:");
                Console.WriteLine(respuesta);
            }
        }

        private async Task GetGroup()
        {
            inputs[0] = inputs.Count > 0 ? inputs[0] : null;
            inputs[1] = inputs.Count > 0 ? inputs[1] : null;

            inputs[0] = GetValidInput("username", verificationsUsername.IsAllowedChars, inputs[0], 0);
            inputs[1] = GetValidInput("password", verificationsPassword.IsValidPassword, inputs[1], 1);

            string username = inputs[0];
            string password = inputs[1];

            inputs.RemoveRange(0, 2);

            string url = $"http://localhost:5046/api/groups/get-group-by-id/{inputs[2]}";

            var (conexionExitosa, respuesta) = await petition.SendRequestAsync(url, HttpMethod.Get, "", inputs, username, password);

            if (conexionExitosa)
            {
                Console.WriteLine("Conexión establecida correctamente.");

                // Si se recibe una respuesta
                if (!string.IsNullOrEmpty(respuesta))
                {
                    Console.WriteLine("Respuesta recibida:");
                    Console.WriteLine(respuesta);
                }
                else
                {
                    Console.WriteLine("No se recibió respuesta del servidor.");
                }
            }
            else
            {
                Console.WriteLine("Error al establecer la conexión o al enviar la solicitud:");
                Console.WriteLine(respuesta);
            }
        }

        private async void GetAllGroup()
        {
            inputs[0] = inputs.Count > 0 ? inputs[0] : null;
            inputs[1] = inputs.Count > 0 ? inputs[1] : null;

            inputs[0] = GetValidInput("username", verificationsUsername.IsAllowedChars, inputs[0], 0);
            inputs[1] = GetValidInput("password", verificationsPassword.IsValidPassword, inputs[1], 1);

            string username = inputs[0];
            string password = inputs[1];

            inputs.RemoveRange(0, 2);

            string url = "http://localhost:5046/api/groups/get-all-group";

            var (conexionExitosa, respuesta) = await petition.SendRequestAsync(url, HttpMethod.Get, "", inputs, username, password);

            if (conexionExitosa)
            {
                Console.WriteLine("Conexión establecida correctamente.");

                // Si se recibe una respuesta
                if (!string.IsNullOrEmpty(respuesta))
                {
                    Console.WriteLine("Respuesta recibida:");
                    Console.WriteLine(respuesta);
                }
                else
                {
                    Console.WriteLine("No se recibió respuesta del servidor.");
                }
            }
            else
            {
                Console.WriteLine("Error al establecer la conexión o al enviar la solicitud:");
                Console.WriteLine(respuesta);
            }
        }

        private string GetValidInput(string fieldName, Func<string, bool> validationMethod, string currentValue, int index)
        {
            string input = currentValue;
            if (currentValue == null || !validationMethod(currentValue))
            {
                do
                {
                    Console.WriteLine($"Enter the {fieldName}:");
                    input = Console.ReadLine();

                } while (!validationMethod(input));
            }

            inputs[index] = input;

            return input;
        }
    }
}
