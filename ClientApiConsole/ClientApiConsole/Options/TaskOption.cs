﻿using ClientApiConsole.Verifications;
using Library;


namespace ClientApiConsole.Options
{
    public class TaskOption
    {
        private string text;
        private List<string> inputs;

        VerificationsUsername verificationsUsername = new VerificationsUsername();
        VerificationsPassword verificationsPassword = new VerificationsPassword();
        VerificationsPriority verificationsPriority = new VerificationsPriority();
        VerificationsDate verificationsDate = new VerificationsDate();
        Petition petition = new Petition();

        public TaskOption(string text, List<string> inputs)
        {
            this.text = text;
            this.inputs = inputs;
            Menu();
        }

        private async Task Menu()
        {
            switch (text)
            {
                case "create-task":
                    CreateTask();

                    break;
                case "delete-task":
                    DeleteTask();

                    break;
                case "get-task":


                    break;
                case "update-task":

                    break;
            }
        }

        private async Task CreateTask()
        {
            inputs[0] = inputs.Count > 0 ? inputs[0] : null;
            inputs[1] = inputs.Count > 0 ? inputs[1] : null;
            inputs[4] = inputs.Count > 0 ? inputs[4] : null;
            inputs[5] = inputs.Count > 0 ? inputs[5] : null;
            inputs[5] = inputs.Count > 0 ? inputs[6] : null;

            inputs[0] = GetValidInput("username", verificationsUsername.IsAllowedChars, inputs[0], 0);
            inputs[1] = GetValidInput("password", verificationsPassword.IsValidPassword, inputs[1], 1);
            inputs[4] = GetValidInput("create date", verificationsDate.IsValidDate, inputs[4], 4);
            inputs[5] = GetValidInput("finish date", verificationsDate.IsValidDate, inputs[5], 5);
            inputs[6] = GetValidInput("priority", verificationsPriority.IsValidPriority, inputs[6], 6);

            string username = inputs[0];
            string password = inputs[1];

            string cuerpoJson = @"
            {
                ""title"": ""?"",
                ""description"": ""?"",
                ""createdDate"": ""?"",
                ""finishDate"": ""?"",
                ""priority"": ""?"",
                ""state"": ""?"",
                ""groupId"": {
                    ""value"": ""?""
                }
            }";

            string url = "http://localhost:5046/api/tasks/create-task";

            var (conexionExitosa, respuesta) = await petition.SendRequestAsync(url, HttpMethod.Post, cuerpoJson, inputs, username, password);

            if (conexionExitosa)
            {
                Console.WriteLine("Conexión establecida correctamente.");

                // Si se recibe una respuesta
                if (!string.IsNullOrEmpty(respuesta))
                {
                    Console.WriteLine("Respuesta recibida:");
                    Console.WriteLine(respuesta);
                }
                else
                {
                    Console.WriteLine("No se recibió respuesta del servidor.");
                }
            }
            else
            {
                Console.WriteLine("Error al establecer la conexión o al enviar la solicitud:");
                Console.WriteLine(respuesta);
            }

        }

        private async Task DeleteTask()
        {
            inputs[0] = inputs.Count > 0 ? inputs[0] : null;
            inputs[1] = inputs.Count > 0 ? inputs[1] : null;

            inputs[0] = GetValidInput("username", verificationsUsername.IsAllowedChars, inputs[0], 0);
            inputs[1] = GetValidInput("password", verificationsPassword.IsValidPassword, inputs[1], 1);

            string username = inputs[0];
            string password = inputs[1];

            inputs.RemoveRange(0, 2);

            string url = $"http://localhost:5046/api/tasks/delete-task-by-id/{inputs[0]}";

            var (conexionExitosa, respuesta) = await petition.SendRequestAsync(url, HttpMethod.Delete, "", inputs, username, password);

            if (conexionExitosa)
            {
                Console.WriteLine("Conexión establecida correctamente.");

                // Si se recibe una respuesta
                if (!string.IsNullOrEmpty(respuesta))
                {
                    Console.WriteLine("Respuesta recibida:");
                    Console.WriteLine(respuesta);
                }
                else
                {
                    Console.WriteLine("No se recibió respuesta del servidor.");
                }
            }
            else
            {
                Console.WriteLine("Error al establecer la conexión o al enviar la solicitud:");
                Console.WriteLine(respuesta);
            }

        }

        private async Task GetTask()
        {
            inputs[0] = inputs.Count > 0 ? inputs[0] : null;
            inputs[1] = inputs.Count > 0 ? inputs[1] : null;

            inputs[0] = GetValidInput("username", verificationsUsername.IsAllowedChars, inputs[0], 0);
            inputs[1] = GetValidInput("password", verificationsPassword.IsValidPassword, inputs[1], 1);

            string username = inputs[0];
            string password = inputs[1];

            inputs.RemoveRange(0, 2);

            string url = $"http://localhost:5046/api/tasks/get-task-by-id/{inputs[0]}";

            var (conexionExitosa, respuesta) = await petition.SendRequestAsync(url, HttpMethod.Get, "", inputs, username, password);

            if (conexionExitosa)
            {
                Console.WriteLine("Conexión establecida correctamente.");

                // Si se recibe una respuesta
                if (!string.IsNullOrEmpty(respuesta))
                {
                    Console.WriteLine("Respuesta recibida:");
                    Console.WriteLine(respuesta);
                }
                else
                {
                    Console.WriteLine("No se recibió respuesta del servidor.");
                }
            }
            else
            {
                Console.WriteLine("Error al establecer la conexión o al enviar la solicitud:");
                Console.WriteLine(respuesta);
            }

        }

        private async Task UpdateTask()
        {
            inputs[0] = inputs.Count > 0 ? inputs[0] : null;
            inputs[1] = inputs.Count > 0 ? inputs[1] : null;
            inputs[3] = inputs.Count > 0 ? inputs[3] : null;

            inputs[0] = GetValidInput("username", verificationsUsername.IsAllowedChars, inputs[0], 0);
            inputs[1] = GetValidInput("password", verificationsPassword.IsValidPassword, inputs[1], 1);
            inputs[3] = GetValidInput("priority", verificationsPriority.IsValidPriority, inputs[3], 3);

            string username = inputs[0];
            string password = inputs[1];

            string cuerpoJson = @"
            {
                ""taskId"": {
                    ""value"": ""?""
                },
                ""priority"": ""?""
            }";

            inputs.RemoveRange(0, 2);

            string url = "http://localhost:5046/api/tasks/update-task-by-id-priority";

            var (conexionExitosa, respuesta) = await petition.SendRequestAsync(url, HttpMethod.Put, cuerpoJson, inputs, username, password);

            if (conexionExitosa)
            {
                Console.WriteLine("Conexión establecida correctamente.");

                // Si se recibe una respuesta
                if (!string.IsNullOrEmpty(respuesta))
                {
                    Console.WriteLine("Respuesta recibida:");
                    Console.WriteLine(respuesta);
                }
                else
                {
                    Console.WriteLine("No se recibió respuesta del servidor.");
                }
            }
            else
            {
                Console.WriteLine("Error al establecer la conexión o al enviar la solicitud:");
                Console.WriteLine(respuesta);
            }
        }

        private string GetValidInput(string fieldName, Func<string, bool> validationMethod, string currentValue, int index)
        {
            string input = currentValue;
            if (currentValue == null || !validationMethod(currentValue))
            {
                do
                {
                    Console.WriteLine($"Enter the {fieldName}:");
                    input = Console.ReadLine();

                } while (!validationMethod(input));
            }

            inputs[index] = input;

            return input;
        }
    }
}
