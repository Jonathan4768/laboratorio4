﻿using ClientApiConsole.Verifications;
using Library;
using System;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace ClientApiConsole.Options
{
    public class UserOption
    {
        private string text;
        private List<string> inputs;

        VerificationsNameAndLastname verificationsNameAndLastname = new VerificationsNameAndLastname();
        VerificationsUsername verificationsUsername = new VerificationsUsername();
        VerificationsEmail verificationsEmail = new VerificationsEmail();
        VerificationsNumber verificationsNumber = new VerificationsNumber();
        VerificationsPassword verificationsPassword = new VerificationsPassword();
        Petition petition = new Petition();


        public UserOption(string text, List<string> inputs)
        {
            this.text = text;
            this.inputs = inputs;

            Menu();
        }

        private void Menu()
        {
            switch (text)
            {
                case "create-user":
                    CreateUser();
                    break;
                case "delete-user":
                    DeleteUser();
                    break;
                case "get-user":
                    GetUser();
                    break;
                case "get-all-user":
                    GetAllUser();
                    break;
                case "update-user":
                    UpdateUser();
                    break;
                case "login-user":
                    LoginUser();
                    break;
                default:
                    Console.WriteLine("Invalid option");
                    break;
            }
        }

        private async Task CreateUser()
        {
            inputs[0] = inputs.Count > 0 ? inputs[0] : null;
            inputs[1] = inputs.Count > 1 ? inputs[1] : null;
            inputs[2] = inputs.Count > 2 ? inputs[2] : null;
            inputs[3] = inputs.Count > 3 ? inputs[3] : null;
            inputs[4] = inputs.Count > 4 ? inputs[4] : null;
            inputs[5] = inputs.Count > 5 ? inputs[5] : null;

            inputs[0] = GetValidInput("name", verificationsNameAndLastname.IsValidText, inputs[0], 0);
            inputs[1] = GetValidInput("lastname", verificationsNameAndLastname.IsValidText, inputs[1], 1);
            inputs[2] = GetValidInput("username", verificationsUsername.IsAllowedChars, inputs[2], 2);
            inputs[3] = GetValidInput("email", verificationsEmail.IsValidEmail, inputs[3], 3);
            inputs[4] = GetValidInput("number", verificationsNumber.IsValidNumber, inputs[4], 4);
            inputs[5] = GetValidInput("password", verificationsPassword.IsValidPassword, inputs[5], 5);


            string cuerpoJson = @"
            {
                ""name"": ""?"",
                ""lastname"": ""?"",
                ""username"": ""?"",
                ""email"": ""?"",
                ""number"": ""?"",
                ""password"": ""?""
            }";

            string url = "http://localhost:5046/api/users/create-user";

            var (conexionExitosa, respuesta) = await petition.SendRequestAsync(url, HttpMethod.Post, cuerpoJson, inputs, "", "");

            if (conexionExitosa)
            {
                Console.WriteLine("Conexión establecida correctamente.");

                // Si se recibe una respuesta
                if (!string.IsNullOrEmpty(respuesta))
                {
                    Console.WriteLine("Respuesta recibida:");
                    Console.WriteLine(respuesta);
                }
                else
                {
                    Console.WriteLine("No se recibió respuesta del servidor.");
                }
            }
            else
            {
                Console.WriteLine("Error al establecer la conexión o al enviar la solicitud:");
                Console.WriteLine(respuesta);
            }

        }

        private async Task DeleteUser()
        {
            inputs[0] = inputs.Count > 0 ? inputs[0] : null;
            inputs[1] = inputs.Count > 0 ? inputs[1] : null;

            inputs[0] = GetValidInput("username", verificationsUsername.IsAllowedChars, inputs[0], 0);
            inputs[1] = GetValidInput("password", verificationsPassword.IsValidPassword, inputs[1], 1);

            string username = inputs[0];
            string password = inputs[1];

            inputs.RemoveRange(0, 2);

            string url = $"http://localhost:5046/api/users/delete-user-by-id/{inputs[0]}";



            var (conexionExitosa, respuesta) = await petition.SendRequestAsync(url, HttpMethod.Delete, "", inputs, username, password);

            if (conexionExitosa)
            {
                Console.WriteLine("Conexión establecida correctamente.");

                // Si se recibe una respuesta
                if (!string.IsNullOrEmpty(respuesta))
                {
                    Console.WriteLine("Respuesta recibida:");
                    Console.WriteLine(respuesta);
                }
                else
                {
                    Console.WriteLine("No se recibió respuesta del servidor.");
                }
            }
            else
            {
                Console.WriteLine("Error al establecer la conexión o al enviar la solicitud:");
                Console.WriteLine(respuesta);
            }

        }

        private async Task GetUser()
        {
            inputs[0] = inputs.Count > 0 ? inputs[0] : null;
            inputs[1] = inputs.Count > 0 ? inputs[1] : null;

            inputs[0] = GetValidInput("username", verificationsUsername.IsAllowedChars, inputs[0], 0);
            inputs[1] = GetValidInput("password", verificationsPassword.IsValidPassword, inputs[1], 1);

            string username = inputs[0];
            string password = inputs[1];

            inputs.RemoveRange(0, 2);

            string urlc = "http://localhost:5046/api/users/get-user-by-id/" + inputs[0];
            string url = urlc;

            foreach (string input in inputs)
            {
                Console.WriteLine(input);
            }

            var (conexionExitosa, respuesta) = await petition.SendRequestAsync(url, HttpMethod.Get, "cuerpoJson", inputs, username, password);

            if (conexionExitosa)
            {
                Console.WriteLine("Conexión establecida correctamente.");

                // Si se recibe una respuesta
                if (!string.IsNullOrEmpty(respuesta))
                {
                    Console.WriteLine("Respuesta recibida:");
                    Console.WriteLine(respuesta);
                }
                else
                {
                    Console.WriteLine("No se recibió respuesta del servidor.");
                }
            }
            else
            {
                Console.WriteLine("Error al establecer la conexión o al enviar la solicitud:");
                Console.WriteLine(respuesta);
            }
        }

        private async Task GetAllUser()
        {
            inputs[0] = inputs.Count > 0 ? inputs[0] : null;
            inputs[1] = inputs.Count > 0 ? inputs[1] : null;

            inputs[0] = GetValidInput("username", verificationsUsername.IsAllowedChars, inputs[0], 0);
            inputs[1] = GetValidInput("password", verificationsPassword.IsValidPassword, inputs[1], 1);

            string username = inputs[0];
            string password = inputs[1];

            string url = "http://localhost:5046/api/users/get-all-user";

            inputs.RemoveRange(0, 2);

            foreach (string input in inputs)
            {
                Console.WriteLine(input);
            }

            var (conexionExitosa, respuesta) = await petition.SendRequestAsync(url, HttpMethod.Get, "", inputs, username, password);

            if (conexionExitosa)
            {
                Console.WriteLine("Conexión establecida correctamente.");

                // Si se recibe una respuesta
                if (!string.IsNullOrEmpty(respuesta))
                {
                    Console.WriteLine("Respuesta recibida:");
                    Console.WriteLine(respuesta);
                }
                else
                {
                    Console.WriteLine("No se recibió respuesta del servidor.");
                }
            }
            else
            {
                Console.WriteLine("Error al establecer la conexión o al enviar la solicitud:");
                Console.WriteLine(respuesta);
            }

        }

        private async Task LoginUser()
        {
            inputs[0] = inputs.Count > 0 ? inputs[0] : null;
            inputs[1] = inputs.Count > 0 ? inputs[1] : null;

            inputs[0] = GetValidInput("username", verificationsUsername.IsAllowedChars, inputs[0], 0);
            inputs[1] = GetValidInput("password", verificationsPassword.IsValidPassword, inputs[1], 1);

            string username = inputs[0];
            string password = inputs[1];

            foreach (string input in inputs)
            {
                Console.WriteLine(input);
            }

            string cuerpoJson = @"
            {
                ""username"": ""?"",
                ""password"": ""?""
            }";

            string url = "http://localhost:5046/api/users/login";

            var (conexionExitosa, respuesta) = await petition.SendRequestAsync(url , HttpMethod.Post, cuerpoJson, inputs, "", "");

            if (conexionExitosa)
            {
                Console.WriteLine("Conexión establecida correctamente.");

                // Si se recibe una respuesta
                if (!string.IsNullOrEmpty(respuesta))
                {
                    Console.WriteLine("Respuesta recibida:");
                    Console.WriteLine(respuesta);
                }
                else
                {
                    Console.WriteLine("No se recibió respuesta del servidor.");
                }
            }
            else
            {
                Console.WriteLine("Error al establecer la conexión o al enviar la solicitud:");
                Console.WriteLine(respuesta);
            }
        }

        private async Task UpdateUser()
        {
            inputs[0] = GetValidInput("username", verificationsUsername.IsAllowedChars, inputs[0], 0);
            inputs[1] = GetValidInput("password", verificationsPassword.IsValidPassword, inputs[1], 1);
            inputs[2] = inputs[2];
            inputs[3] = GetValidInput("new username", verificationsUsername.IsAllowedChars, inputs[3], 3);
            inputs[4] = GetValidInput("email", verificationsEmail.IsValidEmail, inputs[4], 4);
            inputs[5] = GetValidInput("number", verificationsNumber.IsValidNumber, inputs[5], 5);

            string username = inputs[0];
            string password = inputs[1];

            string cuerpoJson = @"
            {
                ""id"": ""?"",
                ""username"": ""?"",
                ""email"": ""?"",
                ""number"": ""?""
            }";

            inputs.RemoveRange(0, 2);

            string url = $"http://localhost:5046/api/users/update-user-by-id-information/{inputs[0]}";

            var (conexionExitosa, respuesta) = await petition.SendRequestAsync(url, HttpMethod.Put, cuerpoJson, inputs, username, password);

            if (conexionExitosa)
            {
                Console.WriteLine("Conexión establecida correctamente.");

                // Si se recibe una respuesta
                if (!string.IsNullOrEmpty(respuesta))
                {
                    Console.WriteLine("Respuesta recibida:");
                    Console.WriteLine(respuesta);
                }
                else
                {
                    Console.WriteLine("No se recibió respuesta del servidor.");
                }
            }
            else
            {
                Console.WriteLine("Error al establecer la conexión o al enviar la solicitud:");
                Console.WriteLine(respuesta);
            }

        }


        private string GetValidInput(string fieldName, Func<string, bool> validationMethod, string currentValue, int index)
        {
            string input = currentValue;
            if (currentValue == null || !validationMethod(currentValue))
            {
                do
                {
                    Console.WriteLine($"Enter the {fieldName}:");
                    input = Console.ReadLine();

                } while (!validationMethod(input));
            }

            inputs[index] = input;

            return input;
        }
    }
}
