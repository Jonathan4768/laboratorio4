﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ClientApiConsole.Verifications
{
    public class VerificationsNumber
    {
        private const string PhoneNumberPattern = @"^\d{10}$";

        public bool IsValidNumber(string number)
        {
            return Regex.IsMatch(number, PhoneNumberPattern);
        }
    }
}
