﻿using System.Text.RegularExpressions;

namespace ClientApiConsole.Verifications
{
    public class CommandVerification
    {
        private static readonly Regex[] Patrones = new Regex[]
        {
            new Regex(@"^create-user \S+ \S+ \S+ \S+ \S+ \S+$"),
            new Regex(@"^delete-user \S+ \S+ \S+$"),
            new Regex(@"^get-user \S+ \S+ \S+$"),
            new Regex(@"^get-all-user \S+ \S+$"),
            new Regex(@"^update-user \S+ \S+ \S+ \S+ \S+ \S+$"),
            new Regex(@"^login-user \S+ \S+$"),
            new Regex(@"^create-group \S+ \S+ \S+ \S+$"),
            new Regex(@"^delete-group \S+ \S+ \S+$"),
            new Regex(@"^get-all-group \S+ \S+$"),
            new Regex(@"^create-task \S+ \S+ \S+ \S+ \S+ \S+ \S+ \S+ \S+$"),
            new Regex(@"^delete-task \S+ \S+ \S+$"),
            new Regex(@"^get-task \S+ \S+ \S+$"),
            new Regex(@"^get-task-priority \S+ \S+ \S+$"),
            new Regex(@"^update-task \S+ \S+ \S+ \S+$"),
            new Regex(@"^get-group \S+ \S+ \S+$")
        };

        public bool VerificationCommands(string comando)
        {
            foreach (var patron in Patrones)
            {
                if (patron.IsMatch(comando))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
