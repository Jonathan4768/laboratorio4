﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ClientApiConsole.Verifications
{
    public class VerificationsPassword
    {
        private const string PasswordPattern = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&]).{8,}$";

        public bool IsValidPassword(string password)
        {
            return Regex.IsMatch(password, PasswordPattern);
        }
    }
}
