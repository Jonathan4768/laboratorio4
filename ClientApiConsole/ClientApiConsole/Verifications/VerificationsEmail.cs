﻿using System.Text.RegularExpressions;

namespace ClientApiConsole.Verifications
{
    public class VerificationsEmail
    {
        private const string EmailPattern = @"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$";

        public bool IsValidEmail(string email)
        {
            return Regex.IsMatch(email, EmailPattern);
        }
    }

}
