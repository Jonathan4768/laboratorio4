﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientApiConsole.Verifications
{
    public class VerificationsPriority
    {
        public enum Priority
        {
            HighPriority,
            MediumPriority,
            LowPriority,
            MinimumPriority,
            NoPriority
        }

        public bool IsValidPriority(string priority)
        {
            Priority parsedPriority;
            if (Enum.TryParse(priority, true, out parsedPriority))
            {
              
                return Enum.IsDefined(typeof(Priority), parsedPriority);
            }

            return false;
        }
    }
}