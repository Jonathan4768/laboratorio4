﻿using System;

namespace ClientApiConsole.Verifications
{
    public class VerificationsDate
    {
        public bool IsValidDate(string dateStr)
        {
            if (DateTime.TryParseExact(dateStr, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out _))
            {
                return true; 
            }

            return false;
        }
    }
}