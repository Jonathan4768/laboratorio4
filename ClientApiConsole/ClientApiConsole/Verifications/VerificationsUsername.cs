﻿namespace ClientApiConsole.Verifications
{
    public class VerificationsUsername
    {
        private const int MinLength = 4;
        private const int MaxLength = 20;
        private const string AllowedChars = "abcdefghijklmnopqrstuvwxyz0123456789_";

        public bool IsAllowedChars(string value)
        {
            if (value.Length < MinLength || value.Length > MaxLength)
            {
                return false;
            }

            string lowercaseValue = value.ToLower();
            string lowercaseAllowedChars = AllowedChars.ToLower();

            foreach (char c in lowercaseValue)
            {
                if (!lowercaseAllowedChars.Contains(c))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
