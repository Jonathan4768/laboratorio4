﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ClientApiConsole.Verifications
{
    public class VerificationsNameAndLastname
    {
        public bool IsValidText(string value)
        {
            Regex regex = new Regex("^[a-zA-Z]+$", RegexOptions.IgnoreCase);

            return regex.IsMatch(value);
        }
    }
}
