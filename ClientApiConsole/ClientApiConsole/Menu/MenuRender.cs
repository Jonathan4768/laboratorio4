﻿namespace ClientApiConsole.Menu
{
    public class MenuRender
    {
        public MenuRender()
        {
            MenuUser();
            MenuGroup();
            MenuTask();

        }
        public void MenuUser()
        {
            Console.WriteLine(
                "Commands user\n\n" +

                "create-user name lastname username email number password\n" +
                "delete-user username password idUser\n" +
                "get-user username password idUser\n" +
                "get-all-user username password\n" +
                "update-user username password idUser username email number\n" +
                "login-user username password\n\n"
            );
        }

        public void MenuGroup()
        {
            Console.WriteLine(
                "Commands group\n\n" +

                "create-group username password title userId\n" +
                "delete-group username password idGroup\n" +
                "get-group username password idGroup\n" +
                "get-all-group username password\n\n"
            );
        }

        public void MenuTask()
        {
            Console.WriteLine(
               "Commands task\n\n" +

                "create-task username password title description createdDate finishDate priority state idGroup\n" +
                "delete-task username password idTask\n" +
                "get-task username password idTask\n" +
                "update-task username password idTask priority\n\n\n"
            );
        }
    }
}
