﻿using ClientApiConsole.Options;

namespace ClientApiConsole.Menu
{
    public class MenuOptions
    {
        private string option;
        private List<string> inputs;

        public MenuOptions(string option, List<string> inputs) 
        {
            this.option = option;
            this.inputs = inputs;
        }

        public void OptionsMenu()
        {
            if (ContainsWord(this.option, "-user"))
            {
                UserOption userOption = new UserOption(this.option, this.inputs);

            } 
            else if (ContainsWord(this.option, "-group"))
            {
                GroupOption groupOption = new GroupOption(this.option, this.inputs);


            }
            else if (ContainsWord(this.option, "-task"))
            {
                TaskOption taskOption = new TaskOption(this.option, this.inputs); 

            }
        }

        static bool ContainsWord(string option, string text)
        {
            return option.IndexOf(text, StringComparison.OrdinalIgnoreCase) >= 0;
        }

    }
}
