﻿using ClientApiConsole.Menu;
using ClientApiConsole.Parser;
using ClientApiConsole.Verifications;

public class Program
{
    static void Main(string[] args)
    {
        MenuRender menuRender = new MenuRender();
        CommandVerification commandVerification = new CommandVerification();

        string command = "";

        while (string.IsNullOrEmpty(command) || !commandVerification.VerificationCommands(command))
        {
            command = Console.ReadLine();
        }

        TextParser textParser = new TextParser(command);

        List<string> inputs = textParser.GetInputs();

        string commandInput = inputs.First();

        if (inputs.Count > 0)
        {
            inputs.RemoveAt(0);
        }

        Console.Clear();

        MenuOptions menuOptions = new MenuOptions(commandInput, inputs);

        menuOptions.OptionsMenu();
    }
}