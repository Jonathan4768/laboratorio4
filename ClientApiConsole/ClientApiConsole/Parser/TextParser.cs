﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientApiConsole.Parser
{
    public class TextParser
    {
        private string command;

        public TextParser(string command)
        {
            this.command = command;
        }

        public List<string> GetInputs()
        {
            string[] inputsArray = this.command.Split(' ');

            List<string> inputsList = new List<string>(inputsArray);

            return inputsList;
        }
    }
}
