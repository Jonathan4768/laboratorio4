﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Librery
{
    public class Petition
    {
        private readonly HttpClient httpClient;

        public Petition()
        {
            this.httpClient = new HttpClient();
        }

        public async Task<string> SendRequestAsync(string url, string metodo, string cuerpoJson = null, List<string> datos = null, string username = null, string password = null)
        {
            try
            {
                cuerpoJson = ReplaceQuestionMarks(cuerpoJson, datos);

                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                {
                    var byteArray = Encoding.ASCII.GetBytes($"{username}:{password}");
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                }

                var request = new HttpRequestMessage(new HttpMethod(metodo), url);
                request.Content = new StringContent(cuerpoJson, Encoding.UTF8, "application/json");

                var response = await httpClient.SendAsync(request);
                Console.ReadKey();
                var responseBody = await response.Content.ReadAsStringAsync();
                Console.ReadKey();

                if (!response.IsSuccessStatusCode)
                {
                    throw new Exception($"Error al enviar la petición: {response.StatusCode}. Mensaje: {responseBody}");
                }

                return responseBody;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error al enviar la petición: {ex.Message}");
            }
        }

        private string ReplaceQuestionMarks(string json, List<string> datos)
        {
            int dataIndex = 0;

            json = Regex.Replace(json, @"\?", m =>
            {

                if (dataIndex < datos.Count)
                {
                    string replacement = datos[dataIndex];
                    dataIndex++;
                    return replacement;
                }
                else
                {
                    return "?";
                }
            });

            return json;
        }
    }
}
