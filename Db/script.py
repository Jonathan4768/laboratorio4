import json
import string
import requests
import random
import re

username = "username1"
password = "Password_1!"

def generate_phone_number():
    return ''.join(random.choices(string.digits, k=10))

def generate_name():
    names = ["Juan", "Jose Luis", "Jose", "Maria Guadalupe", "Francisco", "Guadalupe", "Maria", "Juana", "Antonio", "Jesus", "Miguel Angel", "Pedro", "Alejandro", "Manuel", "Margarita", "Maria Del Carmen", "Juan Carlos", "Roberto", "Fernando", "Daniel", "Carlos", "Jorge", "Ricardo", "Miguel", "Eduardo", "Javier", "Rafael", "Martin", "Raul", "David", "Josefina", "Jose Antonio", "Arturo", "Marco Antonio", "Jose Manuel", "Francisco Javier", "Enrique", "Veronica", "Gerardo", "Maria Elena", "Leticia", "Rosa", "Mario", "Francisca", "Alfredo", "Teresa", "Alicia", "Maria Fernanda", "Sergio", "Alberto", "Luis", "Armando", "Alejandra", "Martha", "Santiago", "Yolanda", "Patricia", "Maria De Los Angeles", "Juan Manuel", "Rosa Maria", "Elizabeth", "Gloria", "Angel", "Gabriela", "Salvador", "Victor Manuel", "Silvia", "María De Guadalupe", "Maria De Jesus", "Gabriel", "Andres", "Oscar", "Guillermo", "Ana Maria", "Ramon", "Maria Isabel", "Pablo", "Ruben", "Antonia", "Maria Luisa", "Luis Angel", "Maria Del Rosario", "Felipe", "Jorge Jesus", "Jaime", "Jose Guadalupe", "Julio Cesar", "Jose De Jesus", "Diego", "Araceli", "Andrea", "Isabel", "Maria Teresa", "Irma", "Carmen", "Lucia", "Adriana", "Agustin", "Maria De La Luz", "Gustavo"]   
    name = random.choice(names)
    name = re.sub(r'[^a-zA-Z\s]', '', name)[:50]
    return name

def generate_lastname():
    lastnames = ["Acosta", "Aguilar", "Aguirre", "Alvarado", "Alvarez", "Avila", "Bautista", "Cabrera", "Camacho", "Campos", "Cardenas", "Carrillo", "Castaneda", "Castillo", "Castro", "Cervantes", "Chavez", "Contreras", "Cortes", "Cruz", "Delgado", "Diaz", "Dominguez", "Espinosa", "Espinoza", "Estrada", "Fernandez", "Flores", "Fuentes", "Garcia", "Gomez", "Gonzalez", "Guerrero", "Gutierrez", "Guzman", "Hernandez", "Herrera", "Huerta", "Ibarra", "Jimenez", "Juarez", "Lara", "Leon", "Lopez", "Luna", "Maldonado", "Marquez", "Martinez", "Medina", "Mejia", "Mendez", "Mendoza", "Meza", "Miranda", "Molina", "Morales", "Moreno", "Munoz", "Nava", "Navarro", "Nunez", "Orozco", "Ortega", "Ortiz", "Pacheco", "Padilla", "Pena", "Pérez", "Ramírez", "Ramos", "Rangel", "Reyes", "Ríos", "Rivera", "Robles", "Rodríguez", "Rojas", "Romero", "Rosales", "Rosas", "Ruiz", "Salas", "Salazar", "Sanchez", "Sandoval", "Santiago", "Santos", "Silva", "Solis", "Soto", "Torres", "Trejo", "Valdez", "Valencia", "Valenzuela", "Vargas", "Vazquez", "Vega", "Velazquez"]
    lastname = random.choice(lastnames)
    lastname = re.sub(r'[^a-zA-Z]', '', lastname)[:50]
    return lastname

def create_user(name, lastname, username, email, number, password):
    payload = {
        "name": name,
        "lastname": lastname,
        "username": username,
        "email": email,
        "number": number,
        "password": password
    }
    response = requests.post("http://localhost:5046/api/users/create-user", json=payload, auth=(username, password))
    print("Código de estado:", response.status_code)
    
    if response.status_code == 200:
        try:
            json_response = response.json()
            print("Respuesta:", json_response)
            return json_response
        except json.decoder.JSONDecodeError:
            print("Error: La respuesta no es un JSON válido")
            print("Contenido de la respuesta:", response.content)
            return None
    else:
        print("Error: La solicitud no fue exitosa")
        print("Contenido de la respuesta:", response.content)
        return None

def get_user_ids(username, password):
    url = "http://localhost:5046/api/users/get-all-user"
    try:
        response = requests.get(url, auth=(username, password))
        response.raise_for_status()
        data = response.json()
        user_ids = [user['id'] for user in data]
        return user_ids
    except requests.exceptions.RequestException as e:
        print("Error al obtener los IDs de los usuarios:", e)
        return None

def next_title(previous_title):
    previous_number = int(previous_title.split("_")[1])
    next_number = previous_number + 1
    new_title = f"Grupo_{next_number}"
    return new_title

def create_group(title, user_id, username, password):
    url = "http://localhost:5046/api/groups/create-group"
    payload = {
        "title": title,
        "userId": {"value": user_id}
    }
    response = requests.post(url, json=payload, auth=(username, password))
    return response

def get_group_ids(username, password):
    url = "http://localhost:5046/api/groups/get-all-group"
    try:
        response = requests.get(url, auth=(username, password))
        response.raise_for_status()
        data = response.json()
        group_ids = [group['id'] for group in data]
        return group_ids
    except requests.exceptions.RequestException as e:
        print("Error al obtener los IDs de los grupos:", e)
        return None

def create_task(task_data, username, password):
    url = "http://localhost:5046/api/tasks/create-task"
    try:
        response = requests.post(url, json=task_data, auth=(username, password))
        response.raise_for_status()
        return response
    except requests.exceptions.RequestException as e:
        print(f"Error al crear la tarea '{task_data['title']}' para el grupo con ID '{task_data['groupId']['value']}':", e)
        return None

def main():
    num_registros = 100

    current_user_ids = get_user_ids(username, password)
    if current_user_ids and len(current_user_ids) >= 99:
        print("Ya hay más de 99 usuarios en el sistema. No se realizarán inserciones.")
        return

    for i in range(num_registros):
        name = generate_name()
        lastname = generate_lastname()
        new_username = f"username{i}"
        email = f"correo{i}@gmail.com"
        number = generate_phone_number()
        new_password = f"Password_{i}!"
        
        create_user(name, lastname, new_username, email, number, new_password)

    user_ids = get_user_ids(username, password)

    if user_ids:
        print("IDs de los usuarios:")
        for user_id in user_ids:
            print(user_id)
        
        last_title = "Grupo_0"
            
        for user_id in user_ids:
            title = next_title(last_title)  

            last_title = title

            response = create_group(title, user_id, username, password)
            if response.status_code == 200:
                print(f"Grupo '{title}' creado exitosamente para el usuario con ID '{user_id}'")
            else:
                print(f"Error al crear el grupo para el usuario con ID '{user_id}'. Código de estado: {response.status_code}")

    group_ids = get_group_ids(username, password)

    priorities = ["HighPriority", "MediumPriority", "LowPriority", "MinimumPriority", "NoPriority"]

    if group_ids:
        for group_id in group_ids:
            for i in range(1, 21):
                task_json = {
                    "title": f"Tarea{i}",
                    "description": f"Descripcion {i}",
                    "createdDate": "17/04/2024",
                    "finishDate": "17/04/2024",
                    "priority": random.choice(priorities),
                    "state": "true",
                    "groupId": {
                        "value": group_id
                    }
                }
            
                response = create_task(task_json, "username1", "Password_1!")
                if response and response.status_code == 200:
                    print(f"Tarea '{task_json['title']}' creada exitosamente para el grupo con ID '{group_id}'")
                elif response:
                    print(f"Error al crear la tarea '{task_json['title']}' para el grupo con ID '{group_id}'. Código de estado: {response.status_code}")
    else:
        print("No se pudo obtener la lista de IDs de grupos.")

if __name__ == "__main__":
    main()
