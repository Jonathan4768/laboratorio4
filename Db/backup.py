import mysql.connector
import datetime
import os

def backup_mysql_database(host, port, user, password, database, output_file):
    connection = None 

    try:
        if os.path.exists(output_file):
            print(f"El archivo de copia de seguridad '{output_file}' ya existe. Actualizando el backup...")
            with open(output_file, 'r') as f:
                existing_backup = f.read()

        connection = mysql.connector.connect(
            host=host,
            port=port,
            user=user,
            password=password,
            database=database
        )

        cursor = connection.cursor()

        current_datetime = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

        with open(output_file, 'w') as f:
            cursor.execute(f"USE `{database}`;")
            cursor.execute("SHOW TABLES;")
            tables = cursor.fetchall()
            for table in tables:
                table_name = table[0]
                cursor.execute(f"SELECT * FROM `{table_name}`;")
                table_data = cursor.fetchall()
                f.write(f"\n-- Table structure for table `{table_name}` --\n\n")
                cursor.execute(f"SHOW CREATE TABLE `{table_name}`;")
                create_table_query = cursor.fetchone()[1]
                f.write(f"{create_table_query};\n\n")
                f.write(f"-- Dumping data for table `{table_name}` --\n\n")
                for row in table_data:
                    row_values = ", ".join([f"'{value}'" if isinstance(value, str) else str(value) for value in row])
                    f.write(f"INSERT INTO `{table_name}` VALUES ({row_values});\n")
            if os.path.exists(output_file) and 'existing_backup' in locals():
                f.write(existing_backup)
        
        print(f"Copia de seguridad de la base de datos '{database}' actualizada con éxito en '{output_file}'")
    except mysql.connector.Error as e:
        print(f"Error al realizar la copia de seguridad: {e}")
    finally:
        if connection is not None and connection.is_connected(): 
            cursor.close()
            connection.close()

if __name__ == "__main__":
    mysql_config = {
        'host': 'localhost',
        'port': '3317',
        'database': 'david.abad.mysql',
        'user': 'root',
        'password': 'development'
    }

    output_file = 'backup.sql'

    backup_mysql_database(**mysql_config, output_file=output_file)