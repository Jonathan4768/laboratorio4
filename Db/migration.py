import sqlite3
import mysql.connector

def create_sqlite_table(sqlite_cursor, table_name, table_info):
    # Crear la consulta de creación de la tabla en SQLite
    create_table_query = f"CREATE TABLE IF NOT EXISTS \"{table_name}\" ("
    for column_info in table_info:
        column_name = column_info[0]
        data_type = column_info[1]
        create_table_query += f"\"{column_name}\" {data_type}, "
    create_table_query = create_table_query[:-2] + ")"  # Eliminar la última coma y espacio
    # Ejecutar la consulta de creación de la tabla en SQLite
    sqlite_cursor.execute(create_table_query)

def migrate_mysql_to_sqlite(mysql_conn, sqlite_conn):
    # Conectar a la base de datos MySQL
    mysql_cursor = mysql_conn.cursor()

    # Conectar a la base de datos SQLite
    sqlite_cursor = sqlite_conn.cursor()

    # Obtener los nombres de las tablas en la base de datos MySQL
    mysql_cursor.execute("SHOW TABLES")
    tables = mysql_cursor.fetchall()

    # Iterar sobre cada tabla y migrar los datos
    for table in tables:
        table_name = table[0]
        # Obtener la estructura de la tabla en MySQL
        mysql_cursor.execute(f"SHOW COLUMNS FROM `{table_name}`")  # Rodear el nombre de la tabla con comillas invertidas (`) para evitar errores de sintaxis
        table_info = mysql_cursor.fetchall()
        # Crear la tabla en SQLite con la misma estructura
        create_sqlite_table(sqlite_cursor, table_name, table_info)

        # Migrar los datos de la tabla
        mysql_cursor.execute(f"SELECT * FROM `{table_name}`")  # Rodear el nombre de la tabla con comillas invertidas (`) para evitar errores de sintaxis
        rows = mysql_cursor.fetchall()

        for row in rows:
            placeholders = ",".join(["?" for _ in range(len(row))])
            sqlite_cursor.execute(f"INSERT INTO \"{table_name}\" VALUES ({placeholders})", row)

    sqlite_conn.commit()
    mysql_conn.close()
    sqlite_conn.close()

def main():
    try:
        mysql_config = {
            'host': 'localhost',
            'port': '3317',
            'database': 'david.abad.mysql',
            'user': 'root',
            'password': 'development'
        }

        sqlite_file = 'david.abad.sqlite'

        mysql_conn = mysql.connector.connect(**mysql_config)

        sqlite_conn = sqlite3.connect(sqlite_file)

        migrate_mysql_to_sqlite(mysql_conn, sqlite_conn)

        print("La migración se ha completado con éxito.")
    except Exception as e:
        print("Se produjo un error durante la migración:", e)

if __name__ == "__main__":
    main()