import subprocess
import os
import threading
import time

def run_docker_compose():
    ruta_docker = os.path.join(os.pardir, 'Db', 'docker')
    os.chdir(ruta_docker)
    try:
        subprocess.run(['docker-compose', 'up', '-d'], check=True)
    except subprocess.CalledProcessError as e:
        with open("log.txt", "a") as file:
            file.write(f"Error al ejecutar el comando 'docker-compose': {e}\n")
    finally:
        os.chdir('..')

def run_dotnet():
    ruta_dotnet = os.path.join(os.pardir, 'Api', 'src', 'Web.API')
    os.chdir(ruta_dotnet)
    try:
        subprocess.run(['dotnet', 'run'], check=True)
    except subprocess.CalledProcessError as e:
        with open("log.txt", "a") as file:
            file.write(f"Error al ejecutar el comando 'dotnet': {e}\n")
    finally:
        os.chdir('..')

def run_script():
    import script
    with open("log.txt", "a") as file:
        file.write("Ejecutando run_script()\n")
    script.main()

def run_backup():
    import backup
    with open("log.txt", "a") as file:
        file.write("Ejecutando run_backup()\n")
    backup.main()

def run_migration():
    import migration
    with open("log.txt", "a") as file:
        file.write("Ejecutando run_migration()\n")
    migration.main()

if __name__ == "__main__":
    docker_thread = threading.Thread(target=run_docker_compose)
    dotnet_thread = threading.Thread(target=run_dotnet)
    docker_thread.start()
    
    docker_thread.join()
    
    dotnet_thread.start()
    with open("log.txt", "a") as file:
        file.write("Iniciando Docker y Dotnet...\n")
    
    time.sleep(20)

    script_thread = threading.Thread(target=run_script)
    backup_thread = threading.Thread(target=run_backup)
    migration_thread = threading.Thread(target=run_migration)

    script_thread.start()
    script_thread.join()
    
    backup_thread.start()
    backup_thread.join()
    
    migration_thread.start()
    migration_thread.join()
    
    with open("log.txt", "a") as file:
        file.write("Todas las funciones han terminado de ejecutarse.\n")