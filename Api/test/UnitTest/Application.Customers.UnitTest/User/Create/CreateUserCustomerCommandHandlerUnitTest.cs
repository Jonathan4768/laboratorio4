using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Customers.User.Create;
using Domain.Customers;
using FluentAssertions;
using Moq;
using Xunit;

namespace Application.Customers.UnitTest.User.Create
{
    public class CreateUserCustomerCommandHandlerUnitTest
    {
        [Fact]
        public async Task Handle_ValidCommand_ReturnsSuccess()
        {

            var userRepositoryMock = new Mock<IUserCustomerRepository>();
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            var handler = new CreateUserCustomerCommandHandler(userRepositoryMock.Object, unitOfWorkMock.Object);
            var command = new CreateUserCustomerCommand
            {
                Name = "John",
                Lastname = "Doe",
                Username = "johndoe",
                Email = "john@example.com",
                Number = "123456789",
                Password = "Password1@"
            };

            var result = await handler.Handle(command, CancellationToken.None);

            result.IsSuccess.Should().BeTrue();
            userRepositoryMock.Verify(repo => repo.Add(It.IsAny<UserCustomer>()), Times.Once);
            unitOfWorkMock.Verify(uow => uow.SaveChangesAsync(CancellationToken.None), Times.Once);
        }

        [Theory]
        [InlineData(null, "Doe", "johndoe", "john@example.com", "123456789", "Password1@", "Name")]
        [InlineData("John", null, "johndoe", "john@example.com", "123456789", "Password1@", "Lastname")]
        [InlineData("John", "Doe", "johndoe", "invalidemail", "123456789", "Password1@", "Email")]
        public async Task Handle_InvalidCommand_ReturnsValidationError(
            string name, string lastname, string username, string email, string number, string password, string expectedErrorField)
        {

            var userRepositoryMock = new Mock<IUserCustomerRepository>();
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            var handler = new CreateUserCustomerCommandHandler(userRepositoryMock.Object, unitOfWorkMock.Object);
            var command = new CreateUserCustomerCommand
            {
                Name = name,
                Lastname = lastname,
                Username = username,
                Email = email,
                Number = number,
                Password = password
            };

            var result = await handler.Handle(command, CancellationToken.None);

            result.IsFailure.Should().BeTrue();
            result.Error.Field.Should().Be(expectedErrorField);
        }

        [Fact]
        public async Task Handle_ExceptionThrown_ReturnsFailureWithError()
        {
            var userRepositoryMock = new Mock<IUserCustomerRepository>();
            userRepositoryMock.Setup(repo => repo.Add(It.IsAny<UserCustomer>())).ThrowsAsync(new Exception("Repository exception"));
            var unitOfWorkMock = new Mock<IUnitOfWork>();
            var handler = new CreateUserCustomerCommandHandler(userRepositoryMock.Object, unitOfWorkMock.Object);
            var command = new CreateUserCustomerCommand();

            var result = await handler.Handle(command, CancellationToken.None);

            result.IsFailure.Should().BeTrue();
            result.Error.Code.Should().Be("CreateTaskCustomer.Failure");
        }
    }
}