using System;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;

namespace Application.Customers.UnitTest.User.Delete
{
    [TestFixture]
    public class DeleteUserCustomerCommandHandlerUnitTest
    {
        [Test]
        public async Task Handle_ExistingCustomerId_ReturnsSuccess()
        {
            var userId = Guid.NewGuid();
            var userRepositoryMock = new Mock<IUserCustomerRepository>();
            var unitOfWorkMock = new Mock<IUnitOfWork>();

            var existingUserCustomer = new UserCustomer(new CustomerId(userId), "John", "Doe", "johndoe", "john@example.com", "1234567890", "Password@123");

            userRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<CustomerId>()))
                            .ReturnsAsync(existingUserCustomer);

            var handler = new DeleteUserCustomerCommandHandler(userRepositoryMock.Object, unitOfWorkMock.Object);
            var command = new DeleteUserCustomerCommand { Id = userId };

            var result = await handler.Handle(command, CancellationToken.None);

            Assert.IsTrue(result.IsSuccess);
            userRepositoryMock.Verify(repo => repo.Delete(existingUserCustomer), Times.Once);
            unitOfWorkMock.Verify(uow => uow.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
        }

        [Test]
        public async Task Handle_NonExistingCustomerId_ReturnsNotFound()
        {
            var userId = Guid.NewGuid();
            var userRepositoryMock = new Mock<IUserCustomerRepository>();
            var unitOfWorkMock = new Mock<IUnitOfWork>();

            userRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<CustomerId>()))
                            .ReturnsAsync((UserCustomer)null);

            var handler = new DeleteUserCustomerCommandHandler(userRepositoryMock.Object, unitOfWorkMock.Object);
            var command = new DeleteUserCustomerCommand { Id = userId };

            // Act
            var result = await handler.Handle(command, CancellationToken.None);

            // Assert
            Assert.IsTrue(result.IsFailure);
            Assert.AreEqual("UserCustomer.NotFound", result.Error.Code);
            userRepositoryMock.Verify(repo => repo.Delete(It.IsAny<UserCustomer>()), Times.Never);
            unitOfWorkMock.Verify(uow => uow.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Never);
        }
    }
}
