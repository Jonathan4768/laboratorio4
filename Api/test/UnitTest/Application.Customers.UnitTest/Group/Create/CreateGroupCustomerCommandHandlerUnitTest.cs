using System;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;

namespace Application.Customers.UnitTest.Group.Create
{
    [TestFixture]
    public class CreateGroupCustomerCommandHandlerUnitTest
    {
        [Test]
        public async Task Handle_ValidCommand_ReturnsSuccess()
        {

            var groupRepositoryMock = new Mock<IGroupCustomerRepository>();
            var unitOfWorkMock = new Mock<IUnitOfWork>();

            var handler = new CreateGroupCustomerCommandHandler(groupRepositoryMock.Object, unitOfWorkMock.Object);

            var command = new CreateGroupCustomerCommand
            {
                Title = "Group Title",
                UserId = Guid.NewGuid()
            };

            var result = await handler.Handle(command, CancellationToken.None);

            Assert.IsTrue(result.IsSuccess);
            groupRepositoryMock.Verify(repo => repo.Add(It.IsAny<GroupCustomer>()), Times.Once);
            unitOfWorkMock.Verify(uow => uow.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
        }

        [Test]
        public async Task Handle_InvalidCommand_ReturnsValidationError()
        {
            var groupRepositoryMock = new Mock<IGroupCustomerRepository>();
            var unitOfWorkMock = new Mock<IUnitOfWork>();

            var handler = new CreateGroupCustomerCommandHandler(groupRepositoryMock.Object, unitOfWorkMock.Object);

            var command = new CreateGroupCustomerCommand
            {
                Title = "",
                UserId = Guid.NewGuid()
            };

            var result = await handler.Handle(command, CancellationToken.None);

            Assert.IsTrue(result.IsFailure);
            Assert.AreEqual("Invalid title value.", result.Error.Message);
            groupRepositoryMock.Verify(repo => repo.Add(It.IsAny<GroupCustomer>()), Times.Never);
            unitOfWorkMock.Verify(uow => uow.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Never);
        }
    }
}