using System;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using Moq;

namespace Application.Customers.UnitTest.Task.Create
{
    [TestFixture]
    public class CreateTaskCustomerCommandHandlerUnitTest
    {
        [Test]
        public async Task Handle_ValidCommand_ReturnsSuccess()
        {
            // Arrange
            var taskRepositoryMock = new Mock<ITaskCustomerRepository>();
            var unitOfWorkMock = new Mock<IUnitOfWork>();

            var handler = new CreateTaskCustomerCommandHandler(taskRepositoryMock.Object, unitOfWorkMock.Object);

            var command = new CreateTaskCustomerCommand
            {
                Title = "Tarea1",
                Description = "Descripcion1",
                CreatedDate = "17/04/2024",
                FinishDate = "17/04/2024",
                Priority = "HighPriority",
                State = "true",
                UserId = Guid.Parse("249260b7-486d-49b0-b6e3-407a33332f7f")
            };

            // Act
            var result = await handler.Handle(command, CancellationToken.None);

            // Assert
            Assert.IsTrue(result.IsSuccess);
            taskRepositoryMock.Verify(repo => repo.Add(It.IsAny<TaskCustomer>()), Times.Once);
            unitOfWorkMock.Verify(uow => uow.SaveChangesAsync(It.IsAny<CancellationToken>()), Times.Once);
        }
    }
}
