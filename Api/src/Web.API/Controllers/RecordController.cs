using Application.Customers.Record.Get;
using MediatR;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Web.API.Controllers
{
    [Route("api/records")]
    public class RecordController : ApiController
    {
        private readonly ISender _mediator;

        public RecordController(ISender mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [HttpGet("get-all-record")]
        public async Task<IActionResult> GetAll()
        {
            var customerResult = await _mediator.Send(new GetAllRecordCustomerCommand());

            return customerResult.Match(
                customers => Ok(customers),
                errors => Problem(errors)
            );
        }
    }
}