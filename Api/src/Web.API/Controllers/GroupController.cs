using Application.Customers.Group.Create;
using Application.Customers.Group.Delete;
using Application.Customers.Group.Get;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Application.Customers.Group.Get.GetAll;
using Microsoft.AspNetCore.Cors;

namespace Web.API.Controllers
{
    [Route("api/groups")]
    public class GroupController : ApiController
    {
        private readonly ISender _mediator;

        public GroupController(ISender mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("create-group")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        public async Task<IActionResult> Create([FromBody] CreateGroupCustomerCommand command)
        {
            var createCustomerResult = await _mediator.Send(command);

            return createCustomerResult.Match(
                customer => Ok(),
                errors => Problem(errors)
            );
        }

        [HttpDelete("delete-group-by-id/{id}")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var deleteResult  = await _mediator.Send(new DeleteGroupCustomerCommand(id));

            return deleteResult.Match(
                customerId => NoContent(),
                errors => Problem(errors)
            );
        }

        [HttpGet("get-group-by-id/{id}")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var customerResult = await _mediator.Send(new GetGroupCustomerCommand(id));

            return customerResult.Match(
                customer => Ok(customer),
                errors => Problem(errors)
            );
        }

        [HttpGet("get-all-group")]
        public async Task<IActionResult> GetAll()
        {
            var customerResult = await _mediator.Send(new GetAllGroupCustomerCommand());
            
            return customerResult.Match(
                customers => Ok(customers),
                errors => Problem(errors)
            );
        }
    }
}