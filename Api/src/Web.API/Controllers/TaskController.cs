using Application.Customers.Group.Get.GetPriority;
using Application.Customers.Task.Create;
using Application.Customers.Task.Delete;
using Application.Customers.Task.Get;
using Domain.ValueObjects.Task;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Application.Customers.Task.Update;
using Microsoft.AspNetCore.Cors;

namespace Web.API.Controllers
{
    [Route("api/tasks")]
    public class TaskController : ApiController
    {
        private readonly ISender _mediator;

        public TaskController(ISender mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("create-task")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        public async Task<IActionResult> Create([FromBody] CreateTaskCustomerCommand command)
        {
            var createCustomerResult = await _mediator.Send(command);

            return createCustomerResult.Match(
                customer => Ok(),
                errors => Problem(errors)
            );
        }

        [HttpDelete("delete-task-by-id/{id}")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var deleteResult = await _mediator.Send(new DeleteTaskCustomerCommand(id));

            return deleteResult.Match(
                customerId => NoContent(),
                errors => Problem(errors)
            );
        }

        [HttpGet("get-task-by-id/{id}")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var customerResult = await _mediator.Send(new GetTaskCustomerCommand(id));

            return customerResult.Match(
                customer => Ok(customer),
                errors => Problem(errors)
            );
        }

        [HttpGet("get-tasks-by-priority/{priority}")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        public async Task<IActionResult> GetByPriority(string priority)
        {
            var priorityObject = Priority.Create(priority);

            if (priorityObject == null)
            {
                return BadRequest("Invalid priority value");
            }

            var customerResult = await _mediator.Send(new GetTaskPriorityCustomerCommand(priorityObject));

            return customerResult.Match(
                customer => Ok(customer),
                errors => Problem(errors)
            );
        }

        [HttpPut("update-task-by-id-priority")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        public async Task<IActionResult> Update([FromBody] UpdateTaskPriorityCommand command)
        {
            var updateResult = await _mediator.Send(command);

            return updateResult.Match(
                customerId => NoContent(),
                errors => Problem(errors)
            );
        }
    }
}