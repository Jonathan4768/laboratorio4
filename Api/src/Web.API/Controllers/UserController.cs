using Application.Customers.User.Create;
using Application.Customers.User.Delete;
using Application.Customers.User.Get;
using Application.Customers.User.Update;
using ErrorOr;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Application.Customers.User.Get.GetAll;
using Microsoft.AspNetCore.Cors;
using Application.Customers.User.Get.GetByUsernameAndPassword;

namespace Web.API.Controllers
{
    [Route("api/users/")]
    public class UserController : ApiController
    {
        private readonly ISender _mediator;

        public UserController(ISender mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        [HttpPost("create-user")]
        public async Task<IActionResult> Create([FromBody] CreateUserCustomerCommand command)
        {
            var createCustomerResult = await _mediator.Send(command);

            return createCustomerResult.Match(
                customer => Ok(),
                errors => Problem(errors)
            );
        }

        [HttpDelete("delete-user-by-id/{id}")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var deleteResult = await  _mediator.Send(new DeleteUserCustomerCommand(id));

            return deleteResult.Match(
                customerId => NoContent(),
                errors => Problem(errors)
            );
        }

        [HttpGet("get-user-by-id/{id}")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var customerResult = await _mediator.Send(new GetUserCustomerCommand(id));

            return customerResult.Match(
                customer => Ok(customer),
                errors => Problem(errors)
            );
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] GetUserAndPasswordCommand command)
        {
            var loginResult = await _mediator.Send(command);

            return loginResult.Match(
                userResponse => Ok(userResponse),
                errors => Problem(errors) 
            );
        }

        [HttpPut("update-user-by-id-information/{id}")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        public async Task<IActionResult> Update(Guid id, [FromBody] UpdateUserCustomerCommand command)
        {
            if(command.Id != id)
            {
                List<Error> errors = new()
                {
                    Error.Validation("Customer.UpdateInvalid", "The request Id does not match with the url Id.")
                };
                return Problem(errors);
            }

            var updateResult = await _mediator.Send(command);

            return updateResult.Match(
                customerId => Ok(updateResult),
                errors => Problem(errors)
            );
        }

        [HttpGet("get-all-user")]
        [Authorize(AuthenticationSchemes = "BasicAuthentication")]
        public async Task<IActionResult> GetAll()
        {
            var customerResult = await _mediator.Send(new GetAllUserCustomerCommand());

            return customerResult.Match(
                customers => Ok(customers),
                errors => Problem(errors)
            );
        }
    }
}