using Application.Data;
using Domain.Customers.Group;
using Domain.Customers.Record;
using Domain.Customers.Task;
using Domain.Customers.User;
using Domain.Primitives;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence
{
    public class ApplicationDbContext : DbContext, IApplicationDbContext, IUnitOfWork
    {
        private readonly IPublisher _publisher;

        public ApplicationDbContext(DbContextOptions options, IPublisher publisher) : base(options)
        {
            _publisher = publisher ?? throw new ArgumentNullException(nameof(publisher));
        }
        public DbSet<UserCustomer> UserCustomers { get; set; }
        public DbSet<TaskCustomer> TaskCustomers { get; set; }
        public DbSet<GroupCustomer> GroupCustomers { get; set; }
        public DbSet<RecordCustomer> RecordCustomers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ApplicationDbContext).Assembly);
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            var domainEvents = ChangeTracker.Entries<Domain.Primitives.AggregateRoot>()
                .Select(e => e.Entity)
                .Where(e => e.GetDomainEvents().Any())
                .SelectMany(e => e.GetDomainEvents());

            var result = await base.SaveChangesAsync(cancellationToken);

            foreach (var evt in domainEvents)
            {
                await _publisher.Publish(evt, cancellationToken);
            }


            return result;
        }
    }
}