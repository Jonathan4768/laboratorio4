using Domain.Customers;
using Domain.Customers.Task;
using Domain.ValueObjects.Task;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Repositories.Task
{
    public class TaskCustomerRepository : ITaskCustomerRepository
    {
        private readonly ApplicationDbContext _context;

        public TaskCustomerRepository(ApplicationDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async System.Threading.Tasks.Task Add(TaskCustomer customer) => await _context.AddAsync(customer);

        public async System.Threading.Tasks.Task Delete(TaskCustomer customer)
        {
            _context.TaskCustomers.Remove(customer);
            await _context.SaveChangesAsync();
        }


        public async Task<TaskCustomer?> GetByIdAsync(CustomerId id)
        {
            return await _context.TaskCustomers.SingleOrDefaultAsync(t => t.Id == id);
        }

        public async Task<IEnumerable<TaskCustomer>> GetByPriorityAsync(Priority priority)
        {
            return await _context.TaskCustomers.Where(t => t.Priority == priority).ToListAsync();
        }

        public async System.Threading.Tasks.Task Update(TaskCustomer customer) => _context.TaskCustomers.Update(customer);
    }
}