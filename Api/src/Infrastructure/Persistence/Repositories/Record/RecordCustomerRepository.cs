using Domain.Customers.Record;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Repositories.Record
{
    public class RecordCustomerRepository : IRecordCustomerRepository
    {
        private ApplicationDbContext _context;

        public RecordCustomerRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async System.Threading.Tasks.Task Add(RecordCustomer customer) => await _context.AddAsync(customer); 

        public async Task<List<RecordCustomer>> GetAll()
        {
            return await _context.RecordCustomers.ToListAsync();
        }
    }
}