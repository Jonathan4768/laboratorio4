using Domain.Customers;
using Domain.Customers.Group;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Repositories.Group
{
    public class GroupCustomerRepository : IGroupCustomerRepository
    {
        private readonly ApplicationDbContext _context;

        public GroupCustomerRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async System.Threading.Tasks.Task Add(GroupCustomer customer) => await _context.AddAsync(customer);

        public async System.Threading.Tasks.Task Delete(GroupCustomer customer)
        {
            _context.GroupCustomers.Remove(customer);
            await _context.SaveChangesAsync();
        }

        public async Task<List<GroupCustomer>> GetAll()
        {
            return await _context.GroupCustomers.ToListAsync();
        }

        public async Task<GroupCustomer?> GetByIdAsync(CustomerId id)
        {
            return await _context.GroupCustomers.FindAsync(id);
        }
    }
}