using Domain.Customers;
using Domain.Customers.User;
using Domain.ValueObjects.User;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Persistence.Repositories.User
{
    public class UserCustomerRepository : IUserCustomerRepository
    {
        private readonly ApplicationDbContext _context;

        public UserCustomerRepository(ApplicationDbContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<UserCustomer?> GetByUsernameAndPasswordAsync(Username username, Password password)
        {
            return await _context.UserCustomers.FirstOrDefaultAsync(u => u.Username == username && u.Password == password);
        }

        async System.Threading.Tasks.Task IUserCustomerRepository.Add(UserCustomer customer)
        {
            await _context.UserCustomers.AddAsync(customer);
            await _context.SaveChangesAsync();
        }

        async System.Threading.Tasks.Task IUserCustomerRepository.Delete(UserCustomer customer)
        {
            _context.UserCustomers.Remove(customer);
            await _context.SaveChangesAsync();
        }

        async Task<bool> IUserCustomerRepository.ExistsAsync(CustomerId id)
        {
            return await _context.UserCustomers.AnyAsync(c => c.Id == id);
        }

        async Task<List<UserCustomer>> IUserCustomerRepository.GetAll()
        {
            return await _context.UserCustomers.ToListAsync();
        }

        async Task<UserCustomer?> IUserCustomerRepository.GetByIdAsync(CustomerId id)
        {
            return await _context.UserCustomers.FindAsync(id);
        }

        async System.Threading.Tasks.Task IUserCustomerRepository.Update(UserCustomer customer) => _context.UserCustomers.Update(customer);
    }
}