using Domain.Customers;
using Domain.Customers.Task;
using Domain.ValueObjects.Task;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configuration.Task
{
    public class TaskCustomerConfiguration : IEntityTypeConfiguration<TaskCustomer>
    {
        public void Configure(EntityTypeBuilder<TaskCustomer> builder)
        {
            builder.ToTable("Task");

            builder.HasKey(t => t.Id);

            builder.Property(t => t.Id)
                .HasConversion(
                    id => id.value,
                    guid => new CustomerId(guid) 
                );

            builder.Property(t => t.Title);
            builder.Property(t => t.Description);
            
            builder.Property(t => t.CreatedDate).HasConversion(
                createdate => createdate.DateString,
                value => Time.Create(value)!);
            
            builder.Property(t => t.FinishDate).HasConversion(
                finishdate => finishdate.DateString,
                value => Time.Create(value)!);

            builder.Property(t => t.Priority)
                .HasConversion(
                    priority => priority.Value.ToString(), 
                    value => Priority.Create(value)!);


            builder.Property(t => t.State).HasConversion(
                state => state.Value,
                value => State.Create(value)!);

            builder.HasOne(g => g.GroupCustomer)
                .WithMany(t => t.Tasks)
                .HasForeignKey(g => g.GroupId);
        }
    }
}