using Domain.Customers;
using Domain.Customers.Record;
using Domain.ValueObjects.Task;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configuration.Record
{
    public class RecordCustomerConfiguration : IEntityTypeConfiguration<RecordCustomer>
    {
        public void Configure(EntityTypeBuilder<RecordCustomer> builder)
        {
            builder.ToTable("Record");

            builder.HasKey(r => r.Id);

            builder.Property(r => r.Id)
                .HasConversion(
                    id => id.value,
                    guid => new CustomerId(guid)
                );

            builder.Property(r => r.Change);

            builder.Property(r => r.ChangeDate).HasConversion(
                changedate => changedate.DateString,
                value => Time.Create(value)!
            );

            builder.HasOne(t => t.TaskCustomer)
                .WithMany(r => r.Records)
                .HasForeignKey(t => t.TaskId);
        }
    }
}