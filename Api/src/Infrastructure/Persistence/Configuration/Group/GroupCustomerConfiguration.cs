using Domain.Customers;
using Domain.Customers.Group;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configuration.Group
{
    public class GroupCustomerConfiguration : IEntityTypeConfiguration<GroupCustomer>
    {
        public void Configure(EntityTypeBuilder<GroupCustomer> builder)
        {
            builder.ToTable("Group");

            builder.HasKey(g => g.Id);

            builder.Property(g => g.Id)
                .HasConversion(
                    id => id.value,
                    guid => new CustomerId(guid)
                );

            builder.Property(g => g.Title);

            builder.HasOne(u => u.UserCustomer)
                .WithMany(g => g.Tasks)
                .HasForeignKey(u => u.UserId);
        }
    }
}