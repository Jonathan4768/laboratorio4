using Domain.Customers;
using Domain.Customers.User;
using Domain.ValueObjects.User;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Infrastructure.Persistence.Configuration
{
    public class UserCustomerConfiguration : IEntityTypeConfiguration<UserCustomer>
    {
        public void Configure(EntityTypeBuilder<UserCustomer> builder)
        {
            builder.ToTable("User");

            builder.HasKey(u => u.Id);

            builder.Property(u => u.Id)
                .HasConversion(
                    id => id.value, 
                    guid => new CustomerId(guid) 
                );

            builder.Property(u => u.Name).HasConversion(
                name => name.Value,
                value => Text.Create(value)!);

            builder.Property(u => u.Lastname).HasConversion(
                lastname => lastname.Value,
                value => Text.Create(value)!);
            
            builder.Property(u => u.Username).HasConversion(
                username => username.Value,
                value => Username.Create(value)!)
                .IsUnicode();

            builder.Property(u => u.Email).HasConversion(
                email => email.Value,
                value => Email.Create(value)!)
                .IsUnicode();

            builder.Property(u => u.Number).HasConversion(
                number => number.Value,
                value => PhoneNumber.Create(value)!)
                .IsUnicode();

            builder.Property(u => u.Password).HasConversion(
                password => password.Value,
                value => Password.Create(value)!);
        }   
        
    }
}