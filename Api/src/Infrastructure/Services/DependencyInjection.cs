using Application.Data;
using Domain.Customers.Group;
using Domain.Customers.Record;
using Domain.Customers.Task;
using Domain.Customers.User;
using Domain.Primitives;
using Infrastructure.Persistence;
using Infrastructure.Persistence.Repositories.Group;
using Infrastructure.Persistence.Repositories.Record;
using Infrastructure.Persistence.Repositories.Task;
using Infrastructure.Persistence.Repositories.User;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Services
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddPersistence(configuration);
            return services;
        }

        private static IServiceCollection AddPersistence(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("DefaultConnection");

            var serverVersion = new MySqlServerVersion(new Version(8, 0, 28));

            services.AddDbContext<ApplicationDbContext>(options => options.UseMySql(connectionString, serverVersion));

            //var connectionString = configuration.GetConnectionString("DefaultConnection");

            // Configuración para SQLite
            //services.AddDbContext<ApplicationDbContext>(options => options.UseSqlite(connectionString));
            
            services.AddScoped<IApplicationDbContext>(sp => sp.GetRequiredService<ApplicationDbContext>());
            services.AddScoped<IUnitOfWork>(sp => sp.GetRequiredService<ApplicationDbContext>());

            services.AddScoped<IUserCustomerRepository, UserCustomerRepository>();
            services.AddScoped<ITaskCustomerRepository, TaskCustomerRepository>();
            services.AddScoped<IGroupCustomerRepository, GroupCustomerRepository>();
            services.AddScoped<IRecordCustomerRepository, RecordCustomerRepository>();
            
            return services;
        }
    }
}