using Domain.Customers.Group;
using Domain.Customers.Task;
using Domain.Customers.User;
using Microsoft.EntityFrameworkCore;

namespace Application.Data
{
    public interface IApplicationDbContext
    {
        DbSet<UserCustomer> UserCustomers { get; set; }
        DbSet<TaskCustomer> TaskCustomers { get; set; }
        DbSet<GroupCustomer> GroupCustomers { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }
}