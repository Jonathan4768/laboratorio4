using Application.Customers.Common;
using Domain.Customers.User;
using ErrorOr;
using MediatR;

namespace Application.Customers.User.Get.GetAll
{
    public sealed class GetAllUserCustomerCommandHandler : IRequestHandler<GetAllUserCustomerCommand, ErrorOr<IReadOnlyList<UserCustomerResponse>>>
    {
        private readonly IUserCustomerRepository _userRepository;

        public GetAllUserCustomerCommandHandler(IUserCustomerRepository userRepository)
        {
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
        }

        public async Task<ErrorOr<IReadOnlyList<UserCustomerResponse>>> Handle(GetAllUserCustomerCommand request, CancellationToken cancellationToken)
        {
            IReadOnlyList<UserCustomer> customers = await _userRepository.GetAll();

            var responses = customers.Select(customer => new UserCustomerResponse(
                customer.Id.value,
                customer.Name.Value,
                customer.Lastname.Value,
                customer.Username.Value,
                customer.Email.Value,
                customer.Number.Value
            )).ToList();

            return responses;
        }
    }
}