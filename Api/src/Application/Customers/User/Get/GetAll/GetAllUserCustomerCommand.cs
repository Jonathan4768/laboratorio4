using Application.Customers.Common;
using ErrorOr;
using MediatR;

namespace Application.Customers.User.Get.GetAll
{
    public record GetAllUserCustomerCommand() : IRequest<ErrorOr<IReadOnlyList<UserCustomerResponse>>>;
}