using Application.Customers.Common;
using ErrorOr;
using MediatR;

namespace Application.Customers.User.Get
{
    public record GetUserCustomerCommand(Guid Id) : IRequest<ErrorOr<UserCustomerResponse>>;
}