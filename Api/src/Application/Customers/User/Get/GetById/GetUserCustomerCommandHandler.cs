using Application.Customers.Common;
using Domain.Customers;
using Domain.Customers.User;
using Domain.Primitives;
using ErrorOr;
using MediatR;

namespace Application.Customers.User.Get
{
    public class GetUserCustomerCommandHandler : IRequestHandler<GetUserCustomerCommand, ErrorOr<UserCustomerResponse>>
    {
        private readonly IUserCustomerRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;

        public GetUserCustomerCommandHandler(IUserCustomerRepository userRepository, IUnitOfWork unitOfWork)
        {
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<ErrorOr<UserCustomerResponse>> Handle(GetUserCustomerCommand query, CancellationToken cancellationToken)
        {
            if (await _userRepository.GetByIdAsync(new CustomerId(query.Id)) is not UserCustomer customer)
            {
                return Error.NotFound("Customer.NotFound", "The customer with the provide Id was not found.");
            }

            return new UserCustomerResponse(
                customer.Id.value,
                customer.Name.Value,
                customer.Lastname.Value,
                customer.Username.Value,
                customer.Email.Value,
                customer.Number.Value
            );
        }
    }
}