using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Customers.Common;
using Domain.Customers.User;
using Domain.Primitives;
using Domain.ValueObjects.User;
using ErrorOr;
using MediatR;

namespace Application.Customers.User.Get.GetByUsernameAndPassword
{
    public class GetUserAndPasswordCommandHandler : IRequestHandler<GetUserAndPasswordCommand, ErrorOr<UserCustomerResponse>>
    {
        private readonly IUserCustomerRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;

        public GetUserAndPasswordCommandHandler(IUserCustomerRepository userRepository, IUnitOfWork unitOfWork)
        {
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<ErrorOr<UserCustomerResponse>> Handle(GetUserAndPasswordCommand query, CancellationToken cancellationToken)
        {
            var username = Username.Create(query.Username);
            var password = Password.Create(query.Password);

            if (username == null || password == null)
            {
                return Error.NotFound("Customer.NotFound", "Invalid username or password format");
            }

            var customer = await _userRepository.GetByUsernameAndPasswordAsync(username, password);

            if (customer == null)
            {
                return Error.NotFound("Customer.NotFound", "Username and password not found or do not match");
            }

            return new UserCustomerResponse(
                customer.Id.value,
                customer.Name.Value,
                customer.Lastname.Value,
                customer.Username.Value,
                customer.Email.Value,
                customer.Number.Value
            );
        }
    }
}