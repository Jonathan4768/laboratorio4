using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Customers.Common;
using ErrorOr;
using MediatR;

namespace Application.Customers.User.Get.GetByUsernameAndPassword
{
    public record GetUserAndPasswordCommand
    (
        string Username,
        string Password
        
    ) : IRequest<ErrorOr<UserCustomerResponse>>;
}