using ErrorOr;
using Domain.Customers.User;
using Domain.Primitives;
using MediatR;
using Domain.Customers;

namespace Application.Customers.User.Delete
{
    public class DeleteUserCustomerCommandHandler : IRequestHandler<DeleteUserCustomerCommand, ErrorOr<Unit>>
    {
        private readonly IUserCustomerRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteUserCustomerCommandHandler(IUserCustomerRepository userRepository, IUnitOfWork unitOfWork)
        {
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<ErrorOr<Unit>> Handle(DeleteUserCustomerCommand command, CancellationToken cancellationToken)
        {
            if(await _userRepository.GetByIdAsync(new CustomerId(command.Id)) is not UserCustomer customer)
            {
                return Error.NotFound("UserCustomer.NotFound", "The customer with the provided Id was not found");
            }

            await _userRepository.Delete(customer);

            await _unitOfWork.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}