using ErrorOr;
using MediatR;

namespace Application.Customers.User.Delete
{
    public record DeleteUserCustomerCommand(Guid Id) : IRequest<ErrorOr<Unit>>;
}