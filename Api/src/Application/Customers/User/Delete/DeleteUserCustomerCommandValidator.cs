using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;

namespace Application.Customers.User.Delete
{
    public class DeleteUserCustomerCommandValidator : AbstractValidator<DeleteUserCustomerCommand>
    {
        public DeleteUserCustomerCommandValidator()
        {
            RuleFor(r =>  r.Id)
                .NotEmpty();
        }
    }
}