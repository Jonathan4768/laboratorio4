using Domain.Customers;
using Domain.Customers.User;
using Domain.Primitives;
using ErrorOr;
using Domain.ValueObjects.User;
using MediatR;

namespace Application.Customers.User.Update
{
    public class UpdateUserCustomerCommandHandler : IRequestHandler<UpdateUserCustomerCommand, ErrorOr<Unit>>
    {
        private readonly IUserCustomerRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateUserCustomerCommandHandler(IUserCustomerRepository userRepository, IUnitOfWork unitOfWork)
        {
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<ErrorOr<Unit>> Handle(UpdateUserCustomerCommand command, CancellationToken cancellationToken)
        {
            var customer = await _userRepository.GetByIdAsync(new CustomerId(command.Id));

            if(!await _userRepository.ExistsAsync(new CustomerId(command.Id)))
            {
                return Error.NotFound("Customer.NotFound", "Thhe customer with the provide Id was not found");

            }

            if (Username.Create(command.Username) is not Username username)
            {
                return Error.Validation("Username", "Invalid username. Please ensure it contains only letters, numbers, and underscores, and has a length between 4 and 20 characters.");
            }

            if (Email.Create(command.Email) is not Email email)
            {
                return Error.Validation("Email", "Invalid email address. Please enter a valid email address.");
            }

            if (PhoneNumber.Create(command.Number) is not PhoneNumber number)
            {
                return Error.Validation("PhoneNumber", "Invalid phone number. Please enter a valid phone number.");
            }

            customer.UpdateCustomer(
                username,
                email,
                number
            );

            await _userRepository.Update(customer);

            await _unitOfWork.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}