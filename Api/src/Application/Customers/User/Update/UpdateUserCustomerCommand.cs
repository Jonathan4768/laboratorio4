using ErrorOr;
using MediatR;

namespace Application.Customers.User.Update
{
    public record UpdateUserCustomerCommand(
        Guid Id,
        string Username,
        string Email,
        string Number

    ) : IRequest<ErrorOr<Unit>>;
}