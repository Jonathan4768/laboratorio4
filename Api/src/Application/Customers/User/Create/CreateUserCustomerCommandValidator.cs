using FluentValidation;

namespace Application.Customers.User.Create
{
    public class CreateUserCustomerCommandValidator : AbstractValidator<CreateUserCustomerCommand>
    {
        public CreateUserCustomerCommandValidator()
        {
            RuleFor(r => r.Name)
                .NotEmpty();

            RuleFor(r => r.Lastname)
                .NotEmpty();

            RuleFor(r => r.Username)
                .NotEmpty();

            RuleFor(r => r.Email)
                .NotEmpty();

            RuleFor(r => r.Number)
                .NotEmpty();
            
            RuleFor(r => r.Password)
                .NotEmpty();
                
        }
    }
}