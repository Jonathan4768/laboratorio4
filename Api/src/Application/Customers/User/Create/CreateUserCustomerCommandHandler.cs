using Domain.Customers;
using Domain.Customers.User;
using Domain.Primitives;
using ErrorOr;
using Domain.ValueObjects.User;
using MediatR;

namespace Application.Customers.User.Create
{
    internal sealed class CreateUserCustomerCommandHandler : IRequestHandler<CreateUserCustomerCommand, ErrorOr<Unit>>
    {
        private readonly IUserCustomerRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CreateUserCustomerCommandHandler(IUserCustomerRepository userCustomerRepository, IUnitOfWork unitOfWork)
        {
            _userRepository = userCustomerRepository ?? throw new ArgumentNullException(nameof(userCustomerRepository));
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        }

        public async Task<ErrorOr<Unit>> Handle(CreateUserCustomerCommand command, CancellationToken cancellationToken)
        {
            try
            {
                if (Text.Create(command.Name) is not Text name)
                {
                    return Error.Validation("Name", "Invalid name. Make sure it contains only letters and does not exceed 50 characters.");
                }

                if (Text.Create(command.Lastname) is not Text lastname)
                {
                    return Error.Validation("Lastname", "Invalid lastname. Make sure it contains only letters and does not exceed 50 characters.");
                }

                if (Username.Create(command.Username) is not Username username)
                {
                    return Error.Validation("Username", "Invalid username. Please ensure it contains only letters, numbers, and underscores, and has a length between 4 and 20 characters.");
                }

                if (Email.Create(command.Email) is not Email email)
                {
                    return Error.Validation("Email", "Invalid email address. Please enter a valid email address.");
                }

                if (PhoneNumber.Create(command.Number) is not PhoneNumber number)
                {
                    return Error.Validation("PhoneNumber", "Invalid phone number. Please enter a valid phone number.");
                }

                if (Password.Create(command.Password) is not Password password)
                {
                    return Error.Validation("Password", "Invalid password. Password must contain at least 8 characters, including at least one uppercase letter, one lowercase letter, one digit, and one special character (@$!%*?&).");
                }

                var userCustomer = new UserCustomer(
                    new CustomerId(Guid.NewGuid()),
                    name,
                    lastname,
                    username,
                    email,
                    number,
                    password
                );

                await _userRepository.Add(userCustomer);

                await _unitOfWork.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
            catch (Exception ex)
            {
                return Error.Failure("CreateTaskCustomer.Failure", ex.Message);
            }

        }
    }
}