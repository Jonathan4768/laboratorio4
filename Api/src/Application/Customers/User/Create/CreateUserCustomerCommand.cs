using ErrorOr;
using MediatR;

namespace Application.Customers.User.Create
{
    public record CreateUserCustomerCommand
    (
        string Name,
        string Lastname,
        string Username,
        string Email,
        string Number,
        string Password   
    ) : IRequest<ErrorOr<Unit>>;
}