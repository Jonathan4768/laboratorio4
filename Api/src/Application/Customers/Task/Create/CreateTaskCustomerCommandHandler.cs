using Domain.Customers.Task;
using Domain.ValueObjects.Task;
using Domain.Primitives;
using ErrorOr;
using MediatR;
using Domain.Customers;

namespace Application.Customers.Task.Create
{
    internal sealed class CreateTaskCustomerCommandHandler : IRequestHandler<CreateTaskCustomerCommand, ErrorOr<Unit>>
    {
        private readonly ITaskCustomerRepository _taskRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CreateTaskCustomerCommandHandler(ITaskCustomerRepository taskRepository, IUnitOfWork unitOfWork)
        {
            _taskRepository = taskRepository ?? throw new ArgumentNullException(nameof(taskRepository));
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
        }

        public async Task<ErrorOr<Unit>> Handle(CreateTaskCustomerCommand command, CancellationToken cancellationToken)
        {
            try
            {
                if (Time.Create(command.CreatedDate) is not Time createdDate)
                {
                    return Error.Validation("CreatedDate", "Invalid created date. Please enter a valid date in the format dd/MM/yyyy.");
                }

                if (Time.Create(command.FinishDate) is not Time finishDate)
                {
                    return Error.Validation("FinishDate", "Invalid finish date. Please enter a valid date in the format dd/MM/yyyy.");
                }

                Priority? priority = null;
                if (Enum.TryParse(command.Priority, out Priority.PriorityType priorityType))
                {
                    priority = Priority.Create(priorityType.ToString());
                    if (priority == null)
                    {
                        return Error.Validation("Priority", "Invalid priority value.");
                    }
                }

                State? state = null;
                if (bool.TryParse(command.State, out var stateValue))
                {
                    state = State.Create(stateValue);
                    if (state == null)
                    {
                        return Error.Validation("State", "Invalid state value.");
                    }
                }

                var taskCustomer = new TaskCustomer(
                    new CustomerId(Guid.NewGuid()),
                    command.Title,
                    command.Description,
                    createdDate,
                    finishDate,
                    priority!,
                    state!,
                    command.GroupId
                );

                await _taskRepository.Add(taskCustomer);

                await _unitOfWork.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
            catch (Exception ex)
            {
                return Error.Failure("CreateUserCustomer.Failure", ex.Message);
            }
        }
    }
}