using MediatR;
using ErrorOr;
using Domain.Customers;

namespace Application.Customers.Task.Create
{
    public record CreateTaskCustomerCommand
    (
        string Title,
        string Description,
        string CreatedDate,
        string FinishDate,
        string Priority,
        string State,
        CustomerId GroupId
        
    ) : IRequest<ErrorOr<Unit>>;
}