using Application.Customers.Common;
using Domain.Customers;
using Domain.Customers.Task;
using Domain.Primitives;
using ErrorOr;
using MediatR;

namespace Application.Customers.Task.Get
{
    public class GetTaskCustomerCommandHandler : IRequestHandler<GetTaskCustomerCommand, ErrorOr<TaskCustomerResponse>>
    {
        private readonly ITaskCustomerRepository _taskRepository;
        private readonly IUnitOfWork _unitOfWork;

        public GetTaskCustomerCommandHandler(ITaskCustomerRepository taskRepository, IUnitOfWork unitOfWork)
        {
            _taskRepository = taskRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<ErrorOr<TaskCustomerResponse>> Handle(GetTaskCustomerCommand query, CancellationToken cancellationToken)
        {
            if(await _taskRepository.GetByIdAsync(new CustomerId(query.id)) is not TaskCustomer customer)
            {
                return Error.NotFound("Customer.NotFound", "The customer with the provide Id was not found.");
            }

            return new TaskCustomerResponse(
                customer.Title,
                customer.Description,
                customer.CreatedDate.ToString(),
                customer.FinishDate.ToString(),
                customer.Priority.ToString(),
                customer.State.ToString()
            );
        }
    }
}