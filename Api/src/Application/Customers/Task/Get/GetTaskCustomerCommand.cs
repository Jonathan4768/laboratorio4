using Application.Customers.Common;
using ErrorOr;
using MediatR;

namespace Application.Customers.Task.Get
{
    public record GetTaskCustomerCommand(Guid id) : IRequest<ErrorOr<TaskCustomerResponse>>;
}