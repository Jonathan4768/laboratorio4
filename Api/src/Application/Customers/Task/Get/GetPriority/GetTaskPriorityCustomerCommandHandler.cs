using Application.Customers.Common;
using ErrorOr;
using MediatR;
using Domain.Customers.Task;
using Domain.Primitives;
using Application.Customers.Group.Get.GetPriority;

namespace Application.Customers.Task.Get.GetPriority
{
    public class GetTaskPriorityCustomerCommandHandler : IRequestHandler<GetTaskPriorityCustomerCommand, ErrorOr<TaskCustomerResponse>>
    {
        private readonly ITaskCustomerRepository _taskRepository;
        private readonly IUnitOfWork _unitOfWork;

        public GetTaskPriorityCustomerCommandHandler(ITaskCustomerRepository taskRepository, IUnitOfWork unitOfWork)
        {
            _taskRepository = taskRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<ErrorOr<TaskCustomerResponse>> Handle(GetTaskPriorityCustomerCommand query, CancellationToken cancellationToken)
        {
            var tasks = await _taskRepository.GetByPriorityAsync(query.Priority);

            if (!tasks.Any())
            {
                return Error.NotFound("Customer.NotFound", "No customers with the provided priority were found.");
            }

            var customer = tasks.First();

            return new TaskCustomerResponse(
                customer.Title,
                customer.Description,
                customer.CreatedDate.ToString(),
                customer.FinishDate.ToString(),
                customer.Priority.ToString(),
                customer.State.ToString()
            );
        }

    }
}