using Application.Customers.Common;
using Domain.ValueObjects.Task;
using ErrorOr;
using MediatR;

namespace Application.Customers.Group.Get.GetPriority
{
    public record GetTaskPriorityCustomerCommand(Priority Priority) : IRequest<ErrorOr<TaskCustomerResponse>>;
}