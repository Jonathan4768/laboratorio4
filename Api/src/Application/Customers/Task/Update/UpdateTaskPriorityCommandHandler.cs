using Domain.Customers.Task;
using Domain.Primitives;
using Domain.ValueObjects.Task;
using ErrorOr;
using MediatR;

namespace Application.Customers.Task.Update
{
    public class UpdateTaskPriorityCommandHandler : IRequestHandler<UpdateTaskPriorityCommand, ErrorOr<Unit>>
    {
        private readonly ITaskCustomerRepository _taskRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UpdateTaskPriorityCommandHandler(ITaskCustomerRepository taskRepository, IUnitOfWork unitOfWork)
        {
            _taskRepository = taskRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<ErrorOr<Unit>> Handle(UpdateTaskPriorityCommand command, CancellationToken cancellationToken)
        {
            var task = await _taskRepository.GetByIdAsync(command.TaskId);

            if (Priority.Create(command.priority) is not Priority priority)
            {
                return Error.Validation("Priority", "Invalid priority");
            }

            task.UpdateCustomer(priority);

            await _taskRepository.Update(task);
            
            await _unitOfWork.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}