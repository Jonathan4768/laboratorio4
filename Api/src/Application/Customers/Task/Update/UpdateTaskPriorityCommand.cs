using Domain.Customers;
using ErrorOr;
using MediatR;

namespace Application.Customers.Task.Update
{
    public record UpdateTaskPriorityCommand
    (
        CustomerId TaskId,
        string priority
    ) : IRequest<ErrorOr<Unit>>;
}