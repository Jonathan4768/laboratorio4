using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ErrorOr;
using MediatR;

namespace Application.Customers.Task.Delete
{
    public record DeleteTaskCustomerCommand(Guid Id) : IRequest<ErrorOr<Unit>>;
}