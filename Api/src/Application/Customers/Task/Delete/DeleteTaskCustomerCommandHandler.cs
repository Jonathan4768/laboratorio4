using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Customers;
using Domain.Customers.Task;
using Domain.Primitives;
using ErrorOr;
using MediatR;

namespace Application.Customers.Task.Delete
{
    public class DeleteTaskCustomerCommandHandler : IRequestHandler<DeleteTaskCustomerCommand, ErrorOr<Unit>>
    {
        private readonly ITaskCustomerRepository _taskRepository;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteTaskCustomerCommandHandler(ITaskCustomerRepository taskRepository, IUnitOfWork unitOfWork)
        {
            _taskRepository = taskRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<ErrorOr<Unit>> Handle(DeleteTaskCustomerCommand command, CancellationToken cancellationToken)
        {
            if(await _taskRepository.GetByIdAsync(new CustomerId(command.Id)) is not TaskCustomer customer)
            {
                return Error.NotFound("TaskCustomer.NotFound", "The customer with the provided Id was not found");
            }

            await _taskRepository.Delete(customer);

            await _unitOfWork.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}