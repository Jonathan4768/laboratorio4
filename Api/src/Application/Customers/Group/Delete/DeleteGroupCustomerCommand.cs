using MediatR;
using ErrorOr;

namespace Application.Customers.Group.Delete
{
    public record DeleteGroupCustomerCommand(Guid Id) : IRequest<ErrorOr<Unit>>;
}