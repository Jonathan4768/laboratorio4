using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Customers;
using Domain.Customers.Group;
using Domain.Primitives;
using ErrorOr;
using MediatR;

namespace Application.Customers.Group.Delete
{
    public class DeleteGroupCustomerCommandHandler : IRequestHandler<DeleteGroupCustomerCommand, ErrorOr<Unit>>
    {
        private readonly IGroupCustomerRepository _groupRepository;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteGroupCustomerCommandHandler(IGroupCustomerRepository groupRepository, IUnitOfWork unitOfWork)
        {
            _groupRepository = groupRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<ErrorOr<Unit>> Handle(DeleteGroupCustomerCommand command, CancellationToken cancellationToken)
        {
            if(await _groupRepository.GetByIdAsync(new CustomerId(command.Id)) is not GroupCustomer customer)
            {
                return Error.NotFound("GroupCustomer.NotFound", "The customer with the provided Id was not found");
            }

            await _groupRepository.Delete(customer);

            await _unitOfWork.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}