using Domain.Customers;
using ErrorOr;
using MediatR;

namespace Application.Customers.Group.Create
{
    public record CreateGroupCustomerCommand
    (
        string Title,
        CustomerId UserId
        
    ) : IRequest<ErrorOr<Unit>>;
}