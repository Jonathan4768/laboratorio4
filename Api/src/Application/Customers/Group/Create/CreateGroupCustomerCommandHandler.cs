using Domain.Customers;
using Domain.Customers.Group;
using Domain.Primitives;
using ErrorOr;
using MediatR;

namespace Application.Customers.Group.Create
{
    public class CreateGroupCustomerCommandHandler : IRequestHandler<CreateGroupCustomerCommand, ErrorOr<Unit>>
    {
        private readonly IGroupCustomerRepository _groupRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CreateGroupCustomerCommandHandler(IGroupCustomerRepository groupRepository, IUnitOfWork unitOfWork)
        {
            _groupRepository = groupRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<ErrorOr<Unit>> Handle(CreateGroupCustomerCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var groupCustomer = new GroupCustomer(
                    new CustomerId(Guid.NewGuid()),
                    command.Title,
                    command.UserId
                );

                await _groupRepository.Add(groupCustomer);

                await _unitOfWork.SaveChangesAsync(cancellationToken);

                return Unit.Value;

            }
            catch(Exception ex)
            {
                return Error.Validation("CreateGroupCustomer.Failure", ex.Message);
            }
        }
    }
}