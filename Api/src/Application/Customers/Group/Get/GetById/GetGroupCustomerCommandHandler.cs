using Application.Customers.Common;
using Domain.Customers;
using Domain.Customers.Group;
using Domain.Primitives;
using ErrorOr;
using MediatR;

namespace Application.Customers.Group.Get
{
    public class GetGroupCustomerCommandHandler : IRequestHandler<GetGroupCustomerCommand, ErrorOr<GroupCustomerResponse>>
    {
        private readonly IGroupCustomerRepository _groupRepository;
        private readonly IUnitOfWork _unitOfWork;

        public GetGroupCustomerCommandHandler(IGroupCustomerRepository groupRepository, IUnitOfWork unitOfWork)
        {
            _groupRepository = groupRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<ErrorOr<GroupCustomerResponse>> Handle(GetGroupCustomerCommand query, CancellationToken cancellationToken)
        {
            if(await _groupRepository.GetByIdAsync(new CustomerId(query.id)) is not GroupCustomer customer)
            {
                return Error.NotFound("Customer.NotFound", "The customer with the provide Id was not found.");
            }
            
            return new GroupCustomerResponse(
                customer.Id.value,
                customer.Title,
                customer.UserId.value
            );
        }
    }
}