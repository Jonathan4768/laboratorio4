using Application.Customers.Common;
using ErrorOr;
using MediatR;

namespace Application.Customers.Group.Get
{
    public record GetGroupCustomerCommand(Guid id) : IRequest<ErrorOr<GroupCustomerResponse>>;
}