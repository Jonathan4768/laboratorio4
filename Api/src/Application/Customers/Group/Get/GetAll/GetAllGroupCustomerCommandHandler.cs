using Application.Customers.Common;
using Domain.Customers.Group;
using ErrorOr;
using MediatR;

namespace Application.Customers.Group.Get.GetAll
{
    public class GetAllGroupCustomerCommandHandler : IRequestHandler<GetAllGroupCustomerCommand, ErrorOr<IReadOnlyList<GroupCustomerResponse>>>
    {

        private readonly IGroupCustomerRepository _groupRepository;

        public GetAllGroupCustomerCommandHandler(IGroupCustomerRepository groupRepository)
        {
            _groupRepository = groupRepository;
        }

        public async Task<ErrorOr<IReadOnlyList<GroupCustomerResponse>>> Handle(GetAllGroupCustomerCommand request, CancellationToken cancellationToken)
        {
            IReadOnlyList<GroupCustomer> customers = await _groupRepository.GetAll();

            var responses = customers.Select(customer => new GroupCustomerResponse(
                customer.Id.value,
                customer.Title,
                customer.UserId.value
            )).ToList();
            
            return responses;
        }
    }
}