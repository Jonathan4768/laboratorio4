using Application.Customers.Common;
using ErrorOr;
using MediatR;

namespace Application.Customers.Group.Get.GetAll
{
    public record GetAllGroupCustomerCommand() : IRequest<ErrorOr<IReadOnlyList<GroupCustomerResponse>>>;
}