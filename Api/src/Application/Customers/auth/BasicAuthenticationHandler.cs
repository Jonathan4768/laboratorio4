using Domain.Customers.User;
using Domain.ValueObjects.User;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;

namespace Application.Customers.auth
{
    public class BasicAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private readonly IUserCustomerRepository _userRepository;

        public BasicAuthenticationHandler(
            IOptionsMonitor<AuthenticationSchemeOptions> options,
            ILoggerFactory logger,
            UrlEncoder encoder,
            ISystemClock clock,
            IUserCustomerRepository userRepository)
            : base(options, logger, encoder, clock)
        {
            _userRepository = userRepository ?? throw new ArgumentNullException(nameof(userRepository));
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.ContainsKey("Authorization"))
                return AuthenticateResult.Fail("Missing Authorization Header");

            string username, password;
            try
            {
                var authHeaderString = Request.Headers["Authorization"].ToString();
                if (string.IsNullOrEmpty(authHeaderString))
                    return AuthenticateResult.Fail("Missing Authorization Header");

                var authHeader = System.Net.Http.Headers.AuthenticationHeaderValue.Parse(authHeaderString);
                if (authHeader == null || authHeader.Parameter == null)
                    return AuthenticateResult.Fail("Invalid Authorization Header");

                var credentialBytes = Convert.FromBase64String(authHeader.Parameter);
                var credentials = Encoding.UTF8.GetString(credentialBytes).Split(new[] { ':' }, 2);

                if (credentials.Length != 2)
                    return AuthenticateResult.Fail("Invalid Authorization Header");

                username = credentials[0];
                password = credentials[1];
            }
            catch
            {
                return AuthenticateResult.Fail("Invalid Authorization Header");
            }

            var user = await _userRepository.GetByUsernameAndPasswordAsync(Username.Create(username)!, Password.Create(password)!);
            if (user == null)
                return AuthenticateResult.Fail("Invalid Username or Password");

            var claims = new[] {
                new Claim(ClaimTypes.Name, username),

            };
            var identity = new ClaimsIdentity(claims, Scheme.Name);
            var principal = new ClaimsPrincipal(identity);
            var ticket = new AuthenticationTicket(principal, Scheme.Name);

            return AuthenticateResult.Success(ticket);
        }
    }
}
