namespace Application.Customers.Common
{
    public record UserCustomerResponse(
        Guid Id,
        string name,
        string lastname,
        string username,
        string email,
        string number
    );
}