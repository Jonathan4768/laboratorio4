using ErrorOr;
using MediatR;

namespace Application.Customers.Common
{
    public record RecordCustomerResponse
    (
        string change,
        string changedate,
        Domain.Customers.CustomerId? taskId) : IRequest<ErrorOr<Unit>>;
}