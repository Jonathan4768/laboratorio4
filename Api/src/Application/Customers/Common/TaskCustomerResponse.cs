
namespace Application.Customers.Common
{
    public record TaskCustomerResponse(
        string title,
        string description,
        string createdDate,
        string finishDate,
        string priority,
        string state
    );
}