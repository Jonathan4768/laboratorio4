namespace Application.Customers.Common
{
    public record GroupCustomerResponse
    (
        Guid Id,
        string title,
        Guid UserId
    );
}