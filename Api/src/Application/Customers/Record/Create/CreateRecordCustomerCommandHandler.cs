using Domain.Customers;
using Domain.Customers.Record;
using Domain.Primitives;
using Domain.ValueObjects.Task;
using ErrorOr;
using MediatR;

namespace Application.Customers.Record.Create
{
    public class CreateRecordCustomerCommandHandler : IRequestHandler<CreateRecordCustomerCommand, ErrorOr<Unit>>
    {
        private readonly IRecordCustomerRepository _recordRepository;
        private readonly IUnitOfWork _unitOfWord;

        public CreateRecordCustomerCommandHandler(IRecordCustomerRepository recordRepository, IUnitOfWork unitOfWord)
        {
            _recordRepository = recordRepository;
            _unitOfWord = unitOfWord;
        }

        public async Task<ErrorOr<Unit>> Handle(CreateRecordCustomerCommand command, CancellationToken cancellationToken)
        {
            try
            {
                if (Time.Create(command.ChangeDate) is not Time changeDate)
                {
                    return Error.Validation("ChangeDate", "Invalid created date. Please enter a valid date in the format dd/MM/yyyy.");
                }

                var recordCustomer = new RecordCustomer(
                    new CustomerId(Guid.NewGuid()),
                    command.Change,
                    changeDate,
                    command.TaskId
                );

                await _recordRepository.Add(recordCustomer);

                await _unitOfWord.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
            catch (Exception ex)
            {
                return Error.Failure("CreateRecordCustomer.Failure", ex.Message);
            }
        }
    }
}