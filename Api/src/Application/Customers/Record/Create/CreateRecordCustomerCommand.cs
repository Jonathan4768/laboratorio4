using Domain.Customers;
using ErrorOr;
using MediatR;

namespace Application.Customers.Record.Create
{
    public record CreateRecordCustomerCommand
    (
        string Change,
        string ChangeDate,
        CustomerId TaskId

    ) : IRequest<ErrorOr<Unit>>;
}