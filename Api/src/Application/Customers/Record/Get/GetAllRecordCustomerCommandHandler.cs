using Application.Customers.Common;
using Domain.Customers.Record;
using ErrorOr;
using MediatR;

namespace Application.Customers.Record.Get
{
    public class GetAllRecordCustomerCommandHandler : IRequestHandler<GetAllRecordCustomerCommand, ErrorOr<IReadOnlyList<RecordCustomerResponse>>>
    {
        private readonly IRecordCustomerRepository _recordRepository;

        public GetAllRecordCustomerCommandHandler(IRecordCustomerRepository recordRepository)
        {
            _recordRepository = recordRepository;
        }

        public async Task<ErrorOr<IReadOnlyList<RecordCustomerResponse>>> Handle(GetAllRecordCustomerCommand request, CancellationToken cancellationToken)
        {
            IReadOnlyList<RecordCustomer> customers = await _recordRepository.GetAll();

            var responses = customers.Select(customer => new RecordCustomerResponse(
                customer.Change,
                customer.ChangeDate.ToString(),
                customer.TaskId
            )).ToList();

            return responses;
        }
    }
}