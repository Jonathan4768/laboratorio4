using Application.Customers.Common;
using ErrorOr;
using MediatR;

namespace Application.Customers.Record.Get
{
    public record GetAllRecordCustomerCommand() : IRequest<ErrorOr<IReadOnlyList<RecordCustomerResponse>>>;
}