using ErrorOr;
using FluentValidation;
using FluentValidation.Results;
using MediatR;

namespace Application.Common.Behaviors
{
    public class ValidationBehavior<IRequest, IResponse> 
    : IPipelineBehavior<IRequest, IResponse>
    where IRequest : IRequest<IResponse>
    where IResponse : IErrorOr
    {
        private readonly IValidator<IRequest>? _validator;

        public ValidationBehavior(IValidator<IRequest>? validator = null)
        {
            _validator = validator;
        }

        public async Task<IResponse> Handle(IRequest request, RequestHandlerDelegate<IResponse> next, CancellationToken cancellationToken)
        {
            if(_validator is null)
            {
                return await next();
            }

            var validatorResult = await _validator.ValidateAsync(request, cancellationToken);

            if(validatorResult.IsValid)
            {
                return await next();
            }

            var errors = validatorResult.Errors
                        .ConvertAll(ValidationFailure => Error.Validation(
                            ValidationFailure.PropertyName,
                            ValidationFailure.ErrorMessage
                        ));
            
            return (dynamic)errors;
        }
    }
}