namespace Domain.Customers.Group
{
    public interface IGroupCustomerRepository
    {
        Task<GroupCustomer?> GetByIdAsync(CustomerId id);
        Task<List<GroupCustomer>> GetAll();
        System.Threading.Tasks.Task Add(GroupCustomer customer);
        System.Threading.Tasks.Task Delete(GroupCustomer customer);
    }
}