using Domain.Customers.Task;
using Domain.Customers.User;
using Domain.Primitives;

namespace Domain.Customers.Group
{
    public class GroupCustomer : AggregateRoot
    {
        public GroupCustomer(CustomerId? id, string title, CustomerId userId)
        {
            Id = id;
            Title = title;
            UserId = userId;
        }

        public CustomerId? Id { get; private set; }
        public string Title { get; private set; } = string.Empty;
        public CustomerId UserId { get; private set; }

        public UserCustomer UserCustomer { get; private set; }

        public ICollection<TaskCustomer> Tasks { get; set; }
    }
}