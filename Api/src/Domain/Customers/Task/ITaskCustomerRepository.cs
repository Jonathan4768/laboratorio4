using Domain.ValueObjects.Task;

namespace Domain.Customers.Task
{
    public interface ITaskCustomerRepository
    {
        Task<TaskCustomer?> GetByIdAsync(CustomerId id);
        Task<IEnumerable<TaskCustomer>> GetByPriorityAsync(Priority priority);
        System.Threading.Tasks.Task Update(TaskCustomer customer);
        System.Threading.Tasks.Task Add(TaskCustomer customer);
        System.Threading.Tasks.Task Delete(TaskCustomer customer);
    }
}