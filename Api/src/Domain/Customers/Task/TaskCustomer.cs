using Domain.Customers.Group;
using Domain.Customers.Record;
using Domain.Primitives;
using Domain.ValueObjects.Task;

namespace Domain.Customers.Task
{
    public class TaskCustomer : AggregateRoot
    {
        public TaskCustomer(CustomerId? id, string title, string description, Time createdDate, Time finishDate, Priority priority, State state, CustomerId groupId)
        {
            Id = id;
            Title = title;
            Description = description;
            CreatedDate = createdDate;
            FinishDate = finishDate;
            Priority = priority;
            State = state;
            GroupId = groupId;
        }

        public CustomerId? Id { get; private set; }
        public string Title { get; private set; } = string.Empty;
        public string Description { get; private set; } = string.Empty;
        public Time CreatedDate { get; private set; }
        public Time FinishDate { get; private set; }
        public Priority Priority { get; private set; }
        public State State { get; private set; }
        public CustomerId GroupId { get; private set; } 
        public GroupCustomer GroupCustomer { get; private set; }
        public ICollection<RecordCustomer> Records { get; set; }

        public void UpdateCustomer(Priority priority)
        {
            Priority = priority;
        }
    }
}