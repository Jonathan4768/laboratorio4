using Domain.Customers.User;

namespace Domain.Customers.Auth
{
    public interface IAuthRepository
    {
        Task<UserCustomer> Login(string username, string password);
        Task<bool> ExistsUser(string username);
    }
}