using Domain.ValueObjects.User;

namespace Domain.Customers.User
{
    public interface IUserCustomerRepository
    {
        System.Threading.Tasks.Task<UserCustomer?> GetByIdAsync(CustomerId id);
        System.Threading.Tasks.Task<List<UserCustomer>> GetAll();
        System.Threading.Tasks.Task<bool> ExistsAsync(CustomerId id);
        System.Threading.Tasks.Task Add(UserCustomer customer);
        System.Threading.Tasks.Task Update(UserCustomer customer);
        System.Threading.Tasks.Task Delete(UserCustomer customer);
        Task<UserCustomer?> GetByUsernameAndPasswordAsync(Username username, Password password);
    }
}