using Domain.Customers.Group;
using Domain.Primitives;
using Domain.ValueObjects.User;

namespace Domain.Customers.User
{
    public class UserCustomer : AggregateRoot
    {
        public UserCustomer(CustomerId id, Text name, Text lastname, Username username, Email email, PhoneNumber number, Password password)
        {
            Id = id ;
            Name = name;
            Lastname = lastname ;
            Username = username ;
            Email = email;
            Number = number;
            Password = password;
            Tasks = new List<GroupCustomer>();
        }
        
        public CustomerId? Id { get; private set; }
        public Text Name { get; private set; }
        public Text Lastname { get; private set; }
        public Username Username { get; private set; }
        public Email Email { get; private set; }
        public PhoneNumber Number { get; private set; }
        public Password Password { get; private set; }
        public ICollection<GroupCustomer> Tasks { get; set; }
        public void UpdateCustomer(Username username, Email email, PhoneNumber number)
        {
            Username = username;
            Email = email;
            Number = number;
        }
    }
}