
namespace Domain.Customers.Record
{
    public interface IRecordCustomerRepository
    {
        Task<List<RecordCustomer>> GetAll();
        System.Threading.Tasks.Task Add(RecordCustomer customer);
    }
}