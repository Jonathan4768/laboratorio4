using Domain.Customers.Task;
using Domain.Primitives;
using Domain.ValueObjects.Task;

namespace Domain.Customers.Record
{
    public class RecordCustomer : AggregateRoot
    {
        public RecordCustomer(CustomerId? id, string change, Time? changeDate, CustomerId? taskId)
        {
            Id = id;
            Change = change;
            ChangeDate = changeDate;
            TaskId = taskId;
        }

        public CustomerId? Id { get; private set; }
        public string Change { get; private set; } = string.Empty;
        public Time ChangeDate { get; private set; }
        public CustomerId? TaskId { get; private set; }
        public TaskCustomer TaskCustomer { get; private set; }
    }
}