using System.Text.RegularExpressions;

namespace Domain.ValueObjects.User
{
public partial record PhoneNumber
    {
        private const string PhoneNumberPattern = @"^\d{10,11}$";
        private const string InternationalPrefixPattern = @"^\+?\d{1,3}$";
        private const string CountryCodePattern = @"^\d{1,3}$";

        private PhoneNumber(string value) => Value = value;

        public string Value { get; set; }

        public static PhoneNumber? Create(string value)
        {
            if (string.IsNullOrEmpty(value) || !IsValidPhoneNumber(value))
            {
                return null;
            }

            return new PhoneNumber(value);
        }

        private static bool IsValidPhoneNumber(string value)
        {
            if (!Regex.IsMatch(value, PhoneNumberPattern))
            {
                return false;
            }

            if (!IsValidInternationalPrefix(value))
            {
                return false;
            }

            if (!IsValidCountryCode(value))
            {
                return false;
            }

            return true;
        }

        private static bool IsValidInternationalPrefix(string value)
        {
            string[] parts = value.Split(new char[] { ' ', '-', '.' });
            if (parts.Length > 1)
            {
                if (!Regex.IsMatch(parts[0], InternationalPrefixPattern))
                {
                    return false;
                }
            }

            return true;
        }

        private static bool IsValidCountryCode(string value)
        {
            string[] parts = value.Split(new char[] { ' ', '-', '.' });
            if (parts.Length > 1)
            {
                if (!Regex.IsMatch(parts[1], CountryCodePattern))
                {
                    return false;
                }
            }

            return true;
        }
    }
}