using System.Text.RegularExpressions;

namespace Domain.ValueObjects.User
{
    public partial record Email
    {
        private const string EmailPattern = @"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$";

        private Email(string value) => Value = value;

        public static Email? Create(string value)
        {
            if(string.IsNullOrEmpty(value) || !EmailRegex.IsMatch(value))
            {
                return null;
            }

            return new Email(value);
        }

        public string Value { get; set; }

        private static readonly Regex EmailRegex = new Regex(EmailPattern);
    }
}