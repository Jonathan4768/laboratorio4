namespace Domain.ValueObjects.User
{
    public partial record Text
    {
        private const int MaxLength = 50;
        private const string AllowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ";

        private Text(string value)
        {
            if (string.IsNullOrEmpty(value) || value.Length > MaxLength || !IsValidText(value))
            {
                throw new ArgumentException("Invalid text value");
            }

            Value = value;
        }

        public string Value { get; }

        public static Text? Create(string value)
        {
            try
            {
                return new Text(value);
            }
            catch (ArgumentException)
            {
                return null;
            }
        }

        private static bool IsValidText(string value)
        {
            foreach (char c in value)
            {
                if (!AllowedChars.Contains(c))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
