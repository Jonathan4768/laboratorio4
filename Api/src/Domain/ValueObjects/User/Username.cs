namespace Domain.ValueObjects.User
{
    public partial record Username
    {
        private const int MinLength = 4;
        private const int MaxLength = 20;
        private const string AllowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";

        private Username(string value) => Value = value;

        public static Username? Create(string value)
        {
            if (string.IsNullOrEmpty(value) || value.Length < MinLength || value.Length > MaxLength)
            {
                return null;
            }

            if (!IsAllowedChars(value))
            {
                return null;
            }

            return new Username(value);
        }

        public string Value { get; set; }

        private static bool IsAllowedChars(string value)
        {
            return value.All(c => AllowedChars.Contains(c));
        }
    }
}
