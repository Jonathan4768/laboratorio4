using System.Text.RegularExpressions;

namespace Domain.ValueObjects.User
{
    public partial record Password
    {
        private const string PasswordPattern = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&]).{8,}$";

        private Password(string value) => Value = value;

        public static Password? Create(string value)
        {
            if(string.IsNullOrEmpty(value) || !PasswordRegex.IsMatch(value))
            {
                return null;
            }

            return new Password(value);
        }

        public string Value { get; set; }

        private static readonly Regex PasswordRegex = new Regex(PasswordPattern);
    }
}