using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.ValueObjects.Task
{
    public partial record State
    {
        private State(bool value)
        {
            Value = value;
        }

        public static State? Create(bool? value)
        {
            if (value == null)
            {
                return null;
            }

            return new State(value.Value);
        }

        public bool Value { get; private set; }
    }
}