namespace Domain.ValueObjects.Task
{
    public partial record Time
    {
        private Time(string dateStr)
        {
            DateString = dateStr;
        }

        public static Time? Create(string dateStr)
        {
            if (string.IsNullOrEmpty(dateStr) || !DateTime.TryParseExact(dateStr, "dd/MM/yyyy", null, System.Globalization.DateTimeStyles.None, out _))
            {
                return null;
            }

            return new Time(dateStr);
        }

        public string DateString { get; private set; }

        public bool IsValid()
        {
            return Create(DateString) != null;
        }
    }
}