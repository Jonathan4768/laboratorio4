namespace Domain.ValueObjects.Task
{    
    public partial record Priority
    {
        public PriorityType Value { get; }

        public Priority(PriorityType value)
        {
            Value = value;
        }

        public static Priority? Create(string value)
        {
            value = value.Trim(); // Eliminar espacios en blanco alrededor de la cadena de prioridad

            if (Enum.TryParse<PriorityType>(value, ignoreCase: true, out var priorityType))
            {
                return new Priority(priorityType);
            }
            return null;
        }


        private static bool IsValidPriority(string value)
        {
            return Enum.TryParse(value, ignoreCase: true, out PriorityType _);
        }

        public enum PriorityType
        {
            HighPriority,
            MediumPriority,
            LowPriority,
            MinimumPriority,
            NoPriority
        }
    }
}