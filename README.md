# Nombre del Proyecto

API C#

## Prerequisitos

--Python
--Docker
--Dotnet

## Instalacion de Python

### Instalación en Ubuntu / Debian

1. Actualiza el índice de paquetes.
    ```
    sudo apt-get update
    ```
2. Instala Python.
    ```
    sudo apt install python3
    ```

3. Verifica que Python se haya instalado correctamente.
    ```
    python3 --version
    ```

### Instalacion en Windows

1. Descargar el instalador de python

    [python3](https://www.python.org/downloads/windows/)

2. Verifica que Python se haya instalado correctamente.
    ```bash
    python3 --version
    ```

## Instalacion de las librerias de python 

1. Instalacion de mysql-connector-python

    ```
    pip install mysql-connector-python
    ```
2. Instalacion de requests

    ```
    pip install requests
    ```

## Ejecución del Proyecto con python

1. Abre una terminal o línea de comandos.

2. Navega hasta la ruta del proyecto:

    ```bash
    cd laboratorio4/Db
    ```

### Para Ubuntu:

3. Ejecuta el siguiente comando para iniciar el contenedor de Docker, iniciar la API de C#, realizar la inserción de datos, el backup y la migración:

    ```bash
    sudo python3 main.py
    ```

### Para Windows:

3. Ejecuta el siguiente comando para iniciar el contenedor de Docker, iniciar la API de C#, realizar la inserción de datos, el backup y la migración:

    ```bash
    python main.py
    ```

4. Para verificar que se haya ejecutado de forma correcta entrar a al siguiente link 

    [API](http://localhost:5046/swagger)


## Levantar solo el contenedor de docker

1. Abre una terminal o línea de comandos.

2. Navega hasta la ruta del proyecto:
    ```bash
    cd laboratorio4/Db/docker
    ```

3. Ejecuta el siguiente comando levantar el contenedor de docker
    ```bash
    docker compose up -d
    ```


## Ejecucion del Proyecto con dotnet

1. Abre una terminal o línea de comandos.

2. Navega hasta la ruta del proyecto:

    ```bash
    cd laboratorio4/Api/src/Web.API
    ```

3. Ejecuta el siguiente comando para iniciar la Api


    ```bash
    dotnet run
    ```

4. Para verificar que se haya ejecutado de forma correcta entrar a al siguiente link 

    [API](http://localhost:5046/swagger)

## Solo ejecucion de la migracion

### Para Ubuntu:
1. Abre una terminal o línea de comandos.

2. Navega hasta la ruta del proyecto:

    ```bash
    cd laboratorio4/Db
    ```

3.  Ejecuta el sigueinte comando para crear la migracion:

    ```
    sudo python3 migration.py
    ```

### Para Windows:
1. Abre una terminal o línea de comandos.

2. Navega hasta la ruta del proyecto:

    ```bash
    cd laboratorio4/Db
    ```
3.  Ejecuta el sigueinte comando para crear la migracion:

    ```
    python migration.py
    ```

## Solo ejecucion del backup

### Para Ubuntu:
1. Abre una terminal o línea de comandos.

2. Navega hasta la ruta del proyecto:

    ```bash
    cd laboratorio4/Db
    ```
3.  Ejecuta el sigueinte comando para crear el backup:

    ```
    sudo python3 backup.py
    ```

4. Y si verificamos los registros en workbench

    MySQL

    Usuarios

    ![alt text](image-7.png)

    Grupos

    ![alt text](image-8.png)

    Tareas

    ![alt text](image-9.png)

    SQLite

    Usuarios

    ![alt text](image-10.png)

    Grupos

    ![alt text](image-11.png)

    Taras

    ![alt text](image-12.png)

Como podemos ver todos los rregistros permanecieron cuando se hizo la migracion 

![alt text](image-13.png)

![alt text](image-14.png)

![alt text](image-15.png)

### Para Windows:
1. Abre una terminal o línea de comandos.

2. Navega hasta la ruta del proyecto:

    ```bash
    cd laboratorio4/Db
    ```
3.  Ejecuta el sigueinte comando para crear el backup:

    ```
    python backup.py
    ```

## Solo ejecucion del script

### Para Ubuntu:
1. Abre una terminal o línea de comandos.

2. Navega hasta la ruta del proyecto:

    ```bash
    cd laboratorio4/Db
    ```
3.  Ejecuta el sigueinte comando para crear el backup:

    ```
    sudo python3 script.py
    ```

### Para Windows:
1. Abre una terminal o línea de comandos.

2. Navega hasta la ruta del proyecto:

    ```bash
    cd laboratorio4/Db
    ```
3.  Ejecuta el sigueinte comando para crear el backup:

    ```
    python script.py
    ```

## Configuracion para uso de SQLite con la api de C#

1. Abre una terminal o línea de comandos.

2. Navega hasta la ruta del proyecto:

    ```
    cd laboratorio4/Api/src/Infrasctructure
    ```

3. Ejecutamos estos comandos

    ```
    dotnet add package Microsoft.EntityFrameworkCore.Sqlite
    ```
    ```
    dotnet add package Microsoft.EntityFrameworkCore.Design
    ```

4. Descomentamos lo que esta comentado para hacer la conexion
    ![alt text](image.png)

5. Despues vamos y ponemos el string de conexion en el app.settings.json

    ![alt text](image-1.png)

6. Despues corremos el comando para ejecutar la migracion

7. El archivo resultante lo movemos a la siguiente direccion

    ```
    cd laboratorio4/Api/src/Infrasctructure
    ```

8. Ejecutamos ya sea con dotnet o con python

    ![alt text](image-2.png)

    ![alt text](image-3.png)

    ![alt text](image-4.png)

    ![alt text](image-5.png)

### Contribución

Si quieres contribuir al proyecto, sigue estos pasos:

1. Haz un fork del proyecto.
2. Crea una nueva rama (`git checkout -b feature/nueva-caracteristica`).
3. Haz commit de tus cambios (`git commit -am 'Agrega una nueva característica'`).
4. Sube tus cambios (`git push origin feature/nueva-caracteristica`).
5. Abre un Pull Request.

## Créditos

Menciona aquí si has utilizado código de otras personas o recursos.

## Licencia

Indica la licencia bajo la cual se distribuye tu proyecto. Por ejemplo:

[MIT](https://opensource.org/licenses/MIT)